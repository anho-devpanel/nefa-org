----------------------------------------------------------------
PHP version
----------------------------------------------------------------
The PHP version is intentionally 7.1, and not 7.2. There may be some conflicts with 7.2
that need to be resolved before switching. Test thoroughly before making the change.

----------------------------------------------------------------
SendGrid and SMTP Authentication Configuration
----------------------------------------------------------------
The site uses a free SendGrid account to send email. An API Key was generated for the account and it
is the key itself which is used as the password in the SMTP Authentication Config to connect to the service.

----------------------------------------------------------------
Custom Salesforce integration
----------------------------------------------------------------

* Saleforce Update (custom module that relies on Salesforce contrib module for basic integration)
* Salesforce - https://www.drupal.org/project/salesforce
  When Salesforce is managed by Composer, be sure to check out the release notes for the available updates after 3.1.
  It potentially requires configuration changes and definitely testing of the custom salesforce modules that
  are used in importing the grant recipient nodes. I noticed too that the 3.1 version of the module installed
  on the site is actually missing a bunch of drush related command files. It was probably intentional because
  as soon as I put them there, Drush stops working because we get the error, The "salesforce_mapping" entity type does not exist.
  We don't have the related submodule enabled because it is not needed. FYI - The error is fixed in 3.2

----------------------------------------------------------------
Developer notes for build/upgrade customizations and solutions
----------------------------------------------------------------

PROJECT: ex. Module Name
ISSUE: ex. the module will not install properly
REFERENCE: ex. http://drupal.org/project/xxxxxx
SOLUTION: ex. we addressed in the following way. . .

---

PROJECT: Block Class Select
Issue: The module is missing its core version requirement for D9
Our Solution: We patched the module but removed it from being managed by Composer. If the module maintainers ever address
the issue, we can have Composer manage it again.

---

PROJECT: Better Exposed Filters
ISSUE: Reset button does not use Ajax.
REFERENCE: https://www.drupal.org/project/better_exposed_filters/issues/2996297
SOLUTION: Applied patch (https://www.drupal.org/files/issues/2021-07-01/better_exposed_filters-reset_button_ajax_5beta1-2996297-53-D8.patch)

---

PROJECT: Block Class Select
ISSUE: The module is missing the core version requirement necessary for D9.
SOLUTION: Applied our own patch (patches/d9-core-version-requirement.diff)

---

PROJECT: Compact date/time range formatter
ISSUE: Format datetime field as date only and Fix bug that is keeping it from working with Optional End Dates
REFERENCES: https://www.drupal.org/project/daterange_compact/issues/2947905 and https://www.drupal.org/project/daterange_compact/issues/2970628
SOLUTION: Created and applied our own patch based on patches provided with the two issues noted above

---

PROJECT: Entity Embed
ISSUE: Modify Entity Embed to allow links on embedded entities.
REFERENCE: Our problem was similar to https://www.drupal.org/project/entity_embed/issues/2511404
SOLUTION: Applied our own local patch (in the patches folder) that would work with a later version of the module based on (https://www.drupal.org/files/issues/entity_embed_links-2511404-20.patch)

---

PROJECT: Media Entity Browser
ISSUE: We want unique media entity browsers for each of our media bundles
REFERENCE: https://www.drupal.org/project/media_entity_browser/issues/2845296
SOLUTION: We created a custom module, "xtra_media_entity_browsers"
NOTES: As Media Entity Browser goes through upgrades, verify our custom module still references the libraries correctly.

---

PROJECT: Paragraphs
ISSUE: Field group support broken with 8.x-1.12 release
REFERENCE: https://www.drupal.org/project/paragraphs/issues/3138609
SOLUTION: Applied patch (https://www.drupal.org/files/issues/2020-05-25/3138609-9.patch)

---
REMOVED THIS PATCH BUT KEEPING THE NOTE
PROJECT: Video Embed Field
ISSUE: Retrieve titles from videos for accessibility
REFERENCE: https://www.drupal.org/project/video_embed_field/issues/2913925
SOLUTION: Applied patch (https://www.drupal.org/files/issues/2018-03-14/retrieve-title-2913925-4.patch)

---
REMOVED THIS PATCH BUT KEEPING THE NOTE
PROJECT: Video Embed Field
ISSUE: Add video title attribute to iFrame for accessibility
REFERENCE:
SOLUTION: Applied our own local patch (patches/add-video-title-attribute-frames.txt)

---
