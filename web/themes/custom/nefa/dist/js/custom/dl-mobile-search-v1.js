!function(i){var e=navigator.userAgent.match(/iPhone/i)?"touchstart":"click";Drupal.behaviors.mobileSearch={attach:function(c){var n=i(".search-icon"),o=i(".block-search .block-inner");n.once().on(e,function(){o.toggleClass("open"),o.find(".form-item > input").focus()}),n.keyup(function(c){13===c.which&&n.click()})}}}(jQuery);
//# sourceMappingURL=dl-mobile-search-v1.js.map
