/**
 * @file
 * A JavaScript file for the mobile menu - version 1.
 * Superfish creates the mobile menu. We append the utility menu options.
 *
 */

(function ($) {
  Drupal.behaviors.mobileMenu = {
    attach: function (context) {

      //$searchBox = $('.search-api-page-block-form').html();
      // $searchBox = $('.search-block-form').html();
      $searchBox = $('.block-search').html();

      $(window).on('load resize orientationchange', function () { // the SF accordion menu needs to load first

        setTimeout(function( event ) {
          //Add the utility menu options to the mobile menu
          if ($('.secondary-nav-item').length < 1) {
            $('.menu--utility-navigation li.menu-item').each( function() {
              $("#superfish-main-accordion").append(
                $(this).clone().addClass('secondary-nav-item')
              );
            });
          }

          //Add search form to top of list
          if ($('.mobile-search').length < 1) {
            $("#superfish-main-accordion").prepend(
              '<li class="mobile-search">' + $searchBox + '</li>'
            );
          }

          // Accessibility: add class to parent ul when mobile menu secondary subnav link has focus
          $('.sf-accordion > .menuparent ul li a').focus(function(){
            $(this).parent().parent().addClass('show-subnav');
          }).blur(function(){
            $(this).parent().parent().removeClass('show-subnav');
          });

          // Accessibility: add class to parent and grandparent ul when mobile menu tertiary subnav link has focus
          $('.sf-accordion > .menuparent ul .menuparent ul li a').focus(function(){
            $(this).parent().parent().parent().parent().addClass('show-subnav');
            $(this).parent().parent().addClass('show-subnav');
          }).blur(function(){
            $(this).parent().parent().parent().parent().removeClass('show-subnav');
            $(this).parent().parent().removeClass('show-subnav');
          });


        }, 100);

      });

    }
  };
})(jQuery);
