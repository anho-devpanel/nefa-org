/**
 * @file
 * A JavaScript file to add classes to nodes if they meet specific criteria.
 *
 */

(function (Drupal, $) {

  Drupal.behaviors.flagNodes = {
    attach: function (context) {

      // Add class to body field if it is empty. It gets rendered if summary is used
      // It would be ideal to do this at the twig level
      $bodyField = $('.node--view-mode-full .field--name-body');
      if ($bodyField.is(':empty')) {
        $bodyField.addClass('empty-body-field');
      }

      // Add class to related view field if it is empty.
      $viewField = $('.field--name-field-related-view');
      if ($viewField.children('.field__item').length < 1) {
        $viewField.addClass('empty-view-field');
      }

      // Add class if there are no captions in the homepage banner tabs
      $homepageBannerTabs = $('#block-homepagebannertabs');
      if ($homepageBannerTabs.length > 0) {
        if ($('.field--name-field-banner-image-caption').length < 1) {
          $homepageBannerTabs.addClass('no-banner-caption');
        }
      }

      // // Add class to reset wrapper
      // // Ideally flag this in a twig file if possible
      // $reset = $('.submit-button #edit-reset');
      // if ($reset.length > 0) {
      //   $reset.parent().addClass('reset-button');
      // }

      // Add class to reset wrapper
      // Ideally flag this in a twig file if possible
      $reset = $("input[data-drupal-selector*='edit-reset']");
      if ($reset.length > 0) {
        $reset.parent().addClass('reset-button');
      }
    }
  };

} (Drupal, jQuery));
