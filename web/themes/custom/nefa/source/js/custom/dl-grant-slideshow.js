/**
 * @file
 * A JavaScript file to turn exposed filter checkboxes into drop downs.
 *
 */

(function (Drupal, $) {

  var event = (navigator.userAgent.match(/iPhone/i)) ? "touchstart" : "click";

  /**
   * Equalize Grant Slideshow
   *
   */
  function equalizeGrantSlideshow() {
    var eqGrant = $('.view-upcomming-grants');
    if(eqGrant.length > 0)  {
      eqGrant.imagesLoaded(function() {
        eqGrant.equalize({children: '.views-row', reset: true });
      });
    }
  }


  Drupal.behaviors.grantSlideshows = {
    attach: function (context) {

      $( window ).on( "load resize orientationchange", function( event ) {
        setTimeout(function( event ) {

          // Equalize Grant Slideshow
          equalizeGrantSlideshow();

        }, 100);
      });

    }
  };

} (Drupal, jQuery));
