/**
 * @file
 * A JavaScript file accordions.
 *
 */

(function (Drupal, $) {

  var event = (navigator.userAgent.match(/iPhone/i)) ? "touchstart" : "click";

  /**
   * Set the accordions
   *
   */
  function setAccordion() {
    $('.accordion-header').each(function() {

      $(this)
        .attr('tabIndex','0') // allow keyboard access
        .off(event)
        .on(event, function(e) {
          $(this).toggleClass('open').next('.accordion-content').slideToggle('500')
            .find('ul li').toggleClass('is-visible'); // Setup for bullet delay
          /* Fix Chrome/FF issue where custom bullet shows up onclick
           * and before lists in accordions are fully visible */
          $('.is-visible')
            .delay(500)
            .queue(function(){
              $(this).addClass('show-bullet');
              $(this).dequeue();
            });
          $('li:not(.is-visible)').removeClass('show-bullet');
        });

      // Accessibility - Open the accordion with enter key if header has keyboard focus
      $(this).keyup(function(e){
        if(e.which === 13){ //13 is the char code for Enter
          $(this).click();
        }
      });

      if ($(this).nextAll('.accordion-content').length === 0) {
        $(this)
          .nextUntil('.accordion-header, .accordion-end')
          .wrapAll('<div class="accordion-content"></div>');
      }
    });
    $('.accordion-content').hide();
  }

  // Add classes to fieldsets that need to behave like accordions
  $('.page-node.node-type-creative-economy-initiative fieldset > legend').addClass('accordion-header');
  $('.page-node.node-type-creative-economy-initiative fieldset > .fieldset__content').addClass('accordion-content');

  // Call function
  setAccordion();

} (Drupal, jQuery));
