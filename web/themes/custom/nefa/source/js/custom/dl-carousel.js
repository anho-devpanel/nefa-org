/**
 * @file
 * This creates the custom carousel/slideshow used by Digital Loom
 */
(function (Drupal, $) {

  /**
   * Set Responsive Slideshow (Homepage Entityqueue Carousel)
   * For configuration see http://kenwheeler.github.io/slick/
   */
  function setBannerTabs() {
    var $tabs = $('.field--name-field-tabs'); //class applied to homepage
    $tabs.each(function(){
      var countItems = $('.field--name-field-tabs', this).length;
      if (countItems > 1) {
        $(this).slick({
          mobileFirst: true,
          arrows: true,
          dots: false,
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          touchMove: true,
          centerPadding: 0,
          autoplay: false,
          autoplaySpeed: 5000,
          speed: 750,
          zIndex: 40,
          fade: true,
          cssEase: 'linear',
          useCSS: true,
          responsive: [
            {
              breakpoint: 640,
              settings: {
                customPaging: function(slider, i) {
                  var title = $(slider.$slides[i]).find('.field--name-field-tab-button-label').text(),
                    slideNumber = (i + 1),
                    totalSlides = slider.slideCount;
                  return '<button type="button" role="tab" aria-label="'+ slideNumber + ' of ' + totalSlides +'" tabindex="0">' + title + '</button>';
                },
                arrows: true,
                dots: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                touchMove: true,
                centerPadding: 0,
                autoplay: false,
                autoplaySpeed: 5000,
                speed: 750,
                zIndex: 40,
                fade: true,
                cssEase: 'linear',
                useCSS: true
              }
            }
          ]
        });

      }
    });
  }

  /**
   * Set Upcoming Grant Deadlines
   * For configuration see http://kenwheeler.github.io/slick/
   */
  function setGrantDeadlines() {
    var $grants = $('.grant-slideshow > .view-content');
    $grants.each(function(){
      var countItems = $('.views-row', this).length;
      if (countItems > 1) {
        $(this).slick({
          mobileFirst: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          centerMode: false,
          arrow: true,
          responsive: [
            // {
            //   breakpoint: 980,
            //   settings: 'unslick'
            // },
            {
              breakpoint: 800,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: false,
                centerMode: false,
                arrow: true,
              }
            },
            {
              breakpoint: 640,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: false,
                centerMode: false,
                arrow: true,
              }
            }
          ]
        });
      }
    });
  }

  /**
   * Set Responsive Statistics
   * For configuration see http://kenwheeler.github.io/slick/
   */
  function setResponsiveStatistics() {
    var $stats = $('.statistics .field--name-field-statistics.field__items');
    $stats.each(function(){
      var countItems = $('.field__item', this).length;
      if (countItems > 1) {
        $(this).slick({
          mobileFirst: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          centerMode: false,
          arrow: true,
          responsive: [
            {
              breakpoint: 980,
              settings: 'unslick'
            },
            {
              breakpoint: 800,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                centerMode: false,
                arrow: true,
              }
            },
            {
              breakpoint: 640,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                centerMode: false,
                arrow: true,
              }
            }
          ]
        });
      }
    });
  }

  function initAllCarousel() {
    setBannerTabs();
    setGrantDeadlines();
    setResponsiveStatistics();
  }

  initAllCarousel();


  // If slideshow needs to be reinitialized at different breakpoints, add it here:
  $( window ).on( "resize orientationchange", function( event ) {
    setTimeout(function( event ) {
      setBannerTabs();
      setGrantDeadlines();
      setResponsiveStatistics();
    }, 100);
  });

})(Drupal, jQuery);
