/**
 * @file
 * A JavaScript file to align banner text with the logo.
 *
 */

(function (Drupal, $) {

  var event = (navigator.userAgent.match(/iPhone/i)) ? "touchstart" : "click";


  function setBannerAlignment() {
    var wi = window.innerWidth,
        $text = $('.not-full .group-text');
    $text.css("left", '');
    $nonNodeTest = $('body:not(.page-node) #page-banner > .container');
    $leftEdge = ($(window).width() - 1250)/2;
    if (wi >= 1250) {
      $text.css("left", $leftEdge);
      $nonNodeTest.css("left", $leftEdge);
    }
  }

  $nonNode = $('body:not(.page-node)');
  if ($nonNode.length > 0) {
    $('#page-banner').append('<div class="group-no-image"><div class="background-design"></div></div>');
  }

  Drupal.behaviors.bannerAlignment = {
    attach: function (context) {

      setBannerAlignment();

      $( window ).on( "load resize orientationchange", function( event ) {
        setTimeout(function( event ) {

          setBannerAlignment();

        }, 100);
      });

    }
  };

} (Drupal, jQuery));
