/**
 * @file
 * A JavaScript file to turn exposed filter checkboxes into drop downs.
 *
 */

(function (Drupal, $) {

  var event = (navigator.userAgent.match(/iPhone/i)) ? "touchstart" : "click";
  
  function setFilterDropdown() {
    $('.filter-dropdown .dropdown-header').each(function() {

      $(this)
        .attr('tabIndex','0') // allow keyboard access
        .off(event)
        .on(event, function(e) {
          $(this).toggleClass('open').next('.dropdown-content').slideToggle('500');
          // But "reset" any other accordion that might already be open
          $('.filter-dropdown .dropdown-header').not(this).removeClass('open')
            .next('.dropdown-content').hide();
        });

      // Accessibility - Open the dropdown with enter key if header has keyboard focus
      $(this).keyup(function(e){
        if(e.which === 13){ //13 is the char code for Enter
          $(this).click();
        }
      });
    });

    if ($('.dropdown-header.open').length < 1) {
      $('.filter-dropdown .dropdown-content').hide();
    }

  }

  function flagFilter(arg) {
    $(arg).each(function() {
      $(this).addClass('filter-dropdown');
      $(this).find('legend').addClass('dropdown-header')
        .next('.fieldset__content').addClass('dropdown-content');
    });
  }


  $refine = $('.form--inline .refine');
  $refineText = "<div class='refine'>Refine</div>";


  flagFilter('fieldset.form-item--id-field_news_type_target_id');
  flagFilter('fieldset.form-item--id-news-author-filter');
  flagFilter('fieldset.form-item--id-field_audience_value');
  flagFilter('fieldset.form-item--id-field_topic_target_id');
  flagFilter('fieldset.form-item--id-field_state_value');
  flagFilter('fieldset.form-item--id-grant_program_filter');
  flagFilter('fieldset.form-item--id-field_fiscal_year_awarded_value');
  flagFilter('fieldset.form-item--id-field_creative_economy_tags_target_id');
  flagFilter('fieldset.form-item--id-field_project_state_value');
  flagFilter('fieldset.form-item--id-field-ccx-presenter-year-target-id');
  flagFilter('fieldset#edit-items-per-page--wrapper');

  setFilterDropdown();

} (Drupal, jQuery));
