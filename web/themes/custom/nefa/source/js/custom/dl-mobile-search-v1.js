/**
 * @file
 * A JavaScript file for the mobile search - version 1.
 *
 */

(function ($) {

  var event = (navigator.userAgent.match(/iPhone/i)) ? "touchstart" : "click";

  Drupal.behaviors.mobileSearch = {
    attach: function (context) {
      var $searchIcon = $('.search-icon'),
        // $searchBox = $('.search-block-form .block-inner');
        //$searchBox = $('.search-api-page-block-form .block-inner');
        $searchBox = $('.block-search .block-inner');

      $searchIcon.once().on(event,function(){
        $searchBox.toggleClass( "open" ); // uncomment if no slide effect is desired
        $searchBox.find('.form-item > input').focus(); // uncomment if no slide effect is desired
        // $searchBox.slideToggle( "slow", function() { //comment out if no slide effect is desired
        //   //$searchBox.find('input.form-search').focus();
        //   $searchBox.find('input.#edit-search-api-fulltext').focus();
        // });
      });

      // Accessibility - Open the search box with enter key if icon has keyboard focus
      $searchIcon.keyup(function(e){
        if(e.which === 13){ //13 is the char code for Enter
          $searchIcon.click();
        }
      });

    }
  };

})(jQuery);
