/**
 * @file
 * A shell for small custom utilities for the theme. Anything of substance
 * should be separated into its own js file.
 *
 */

(function (Drupal, $) {
  
  var event = (navigator.userAgent.match(/iPhone/i)) ? "touchstart" : "click";

  // code here


  Drupal.behaviors.themeUtilities = {
    attach: function (context) {

      // code here

      $( window ).on( "load resize orientationchange", function( event ) {
        setTimeout(function( event ) {

          // code here

        }, 100);
      });

    }
  };

} (Drupal, jQuery));
