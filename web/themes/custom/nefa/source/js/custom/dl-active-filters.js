/**
 * @file
 * A JavaScript file to display the active exposed filters
 *
 */

(function (Drupal, $) {

  Drupal.behaviors.activeFilters = {
    attach: function (context) {

      var $filteredOutput = $('.filtered-output');
      if ($filteredOutput.length < 1) {
        var $showFilters = $('.show-active-filters'),
          selected = [];
        if ($showFilters.length > 0) {
          $('.bef-exposed-form .js-form-type-checkbox input:checked').each(function() {
            selected.push($(this).next('label').text());
          });
          if (selected.length > 0) {
            var $filters = "<div class='filtered-output'><span class='filtered-by-label'>Filtered by:</span> " + selected + "</div>";
            $showFilters.prepend($filters);
          }
        }
      }
    }
  };

} (Drupal, jQuery));
