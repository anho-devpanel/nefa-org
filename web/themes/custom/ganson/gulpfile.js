var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    strip = require('gulp-strip-comments'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    jsvalidate = require('gulp-jsvalidate');

/* Helper functions */
gulp.task('validateJs', function() { // Just validate our js noted by "dl-" naming prefix
  return gulp.src(['./js/custom/*.js'])
    .pipe(jsvalidate());
});

/* Process sass */
gulp.task('processSass', function () {
  gulp.src('./source/sass/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(autoprefixer('last 2 version'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./dist/css'));
});

/* Process javascript */
gulp.task("processJs", ['validateJs'], function() {
  gulp.src(['./source/js/custom/*', './source/js/contrib/*'])
    .pipe(sourcemaps.init())
    .pipe(strip())
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./dist/js'));
});

/* Watch */
gulp.task('watch', function(){
  gulp.watch(['./source/sass/*', './source/sass/*/*', './source/sass/*/*/*'], ['processSass']);
  gulp.watch(['./source/js/custom/*.js','./source/js/contrib/*.js'], ['processJs']);
});
