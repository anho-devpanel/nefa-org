<?php

/**
 * @file
 * Preprocess functions for ganson.
 */

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Entity;

/**
 * Prepares variables for the html.html.twig template.
 */
function ganson_preprocess_html(&$variables) {
  try {
    $variables['is_front'] = \Drupal::service('path.matcher')->isFrontPage();
  }
  catch (Exception $e) {
    // If the database is not yet available, set default values for these
    // variables.
    $variables['is_front'] = FALSE;
  }

  // Add classes for node id and menu section.
  if (!$variables['is_front']) {
    $path = \Drupal::service('path.current')->getPath();
    $alias = \Drupal::service('path_alias.manager')->getAliasByPath($path);
    $alias = trim($alias, '/');
    if (!empty($alias)) {
      list($section,) = explode('/', $alias, 2);
      if (!empty($section)) {
        $variables['attributes']['class'][] = 'section-' . $section;
      }
    }
    if ($node = \Drupal::request()->attributes->get('node')) {
      $variables['attributes']['class'][] = 'node-' . $node->id();
    }
  }

  // Add cachability metadata.
  $theme_name = \Drupal::theme()->getActiveTheme()->getName();
  $theme_settings = \Drupal::config($theme_name . '.settings');
  CacheableMetadata::createFromRenderArray($variables)
    ->addCacheableDependency($theme_settings)
    ->applyTo($variables);
  // Union all theme setting variables to the html.html.twig template.
  $variables += $theme_settings->getOriginal();
}

/**
 * Prepares variables for the field.html.twig template.
 */
function ganson_preprocess_field(&$variables, $hook) {
  // Make additional variables available to the template.
  $variables['bundle'] = $variables['element']['#bundle'];
}

/**
 * Implements hook_form_alter().
 */
function ganson_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // Add placeholder text to search block form
  if ($form_id == 'search_block_form') {
    $form['keys']['#attributes']['placeholder'] = t('Search');
  }
}

/**
 * Prepares variables for the search-result.html.twig template.
 *
 */
function ganson_preprocess_search_result(&$variables) {
  $result = $variables['result'];
  // Content Type variable
  if (isset($result['type'])) {
    $variables['content_type'] = $result['type'];
  }
  // Read More variable
  $node = $result['node'];
  $url = $node->nid->value;
  $options = array(
    'attributes' => ['class' => ['read-more']],
    'absolute'   => TRUE,
  );
  $variables['read'] = Link::fromTextAndUrl(t('More'), Url::fromUri('internal:/node/' . $url, $options))->toString();
}
