/**
 * @file
 * A JavaScript file for the theme.
 *
 */

(function (Drupal, $) {
  
  var event = (navigator.userAgent.match(/iPhone/i)) ? "touchstart" : "click";
  
  /**
   * Add button to close message boxes
   *
   */
  function closeMessageBox() {
    $('div.messages').each(function(){
      var $box = $(this);
      $box.append('<div class="bt-close-message"></div>');
      $(".bt-close-message", this)
        .off(event)
        .on(event, function(e) {
          $box.remove();
        });
    });
  }
  
  // Add space before read more links
  $(".field--name-body div.more-link").before("&nbsp;"); // smarttrimmed body

  // Call functions
  closeMessageBox();

  Drupal.behaviors.dlUtilities = {
    attach: function (context) {

      $( window ).on( "load", function( event) {
        // Remove the .fouc class that was added to elements to prevent the flash-of-unstyled-content
        $(".fouc").removeClass("fouc");
      });

      $( window ).on( "load resize orientationchange", function( event ) {
        setTimeout(function( event ) {
          // Add code here
        }, 100);
      });

    }
  };

} (Drupal, jQuery));

//# sourceMappingURL=dl-utilities.js.map
