<?php

namespace Drupal\salesforce_update\Query;

/**
 * @file
 * Contains the SalesforceUpdateQuery class.
 */

use Drupal\salesforce\SelectQuery;
use Drupal\salesforce\SelectQueryResult;
use Psr\Log\LogLevel;

/**
 * @class SalesforceUpdateQuery
 * Manages and executes the query to Salesforce for nefa.org
 */
class SalesforceUpdateQuery {

  //Our api connector
  private $sfApi;

  //Our soql query
  private $soql;

  //The url for the next set of records
  private $next;

  //The indicator that there are more records
  public $more = FALSE;

  /**
   * Constructor -- inits the soql object
   */
  public function __construct($sfObject) {
    $this->sfApi = \Drupal::service('salesforce.client');
    $this->soql = new SelectQuery($sfObject);
  }

  /**
   * Sets conditions on the query
   */
  protected function addCondition($field, $value, $operator = "=") {
    switch (gettype($value)) {
      case "string":
        if ($field !== "LastModifiedDate") {
          $this->encodeString($value);
        }
        break;
      case "array":
        //Apply encodeString to every value
        array_walk($value, [$this, "encodeString"], TRUE);
      default:
        break;
    }
    $this->soql->addCondition($field, $value, $operator);
  }

  /**
   * Encodes strings for soql queries
   *
   * @param str|int $value The value to encode for soql
   * @param str $key The key, if the value is coming from an array
   * @param bool $fromArray True if coming from an array
   */
  private function encodeString(&$value, $key = "", $fromArray = FALSE) {
    if (!is_string($value) || $value === "null") {
      return;
    }
    $value = str_replace([" ", '"', "'"], ["+", "", ""], $value);
    //The salesforce api adds quotes to array values, but not string values
    if (!$fromArray) {
      $value = "'" . $value . "'";
    }
  }

  /**
   * Adds fields to the query
   */
  protected function addFields($fields) {
    if (!is_array($fields) || empty($fields)) {
      $msg = "Fields must be passed as an non-empty array.";
      _salesforce_update_error($msg, $fields, __CLASS__, __METHOD__);
      return FALSE;
    }
    $this->soql->fields = $fields;
  }

  /**
   * Sets the limit on the query.
   */
  public function setLimit($num) {
    if (!is_int($num)) {
      $msg = "Argument is not an integer.";
      _salesforce_update_error($msg, $num, __CLASS__, __METHOD__);
      return FALSE;
    }
    $this->soql->limit = $num;
    return TRUE;
  }

  /**
   * Runs the Salesforce query, using the two classes from Salesforce module.
   *
   * @return \Drupal\salesforce\SelectQueryResult The results!
   */
  public function query() {
    try {
      /**
       * @var \Drupal\salesforce\SelectQueryResult
       */
      $results = $this->sfApi->query($this->soql);
    } catch (Exception $exc) {
      watchdog("Salesforce Update", $exc->getMessage());
      return FALSE;
    }

    if ($results->nextRecordsUrl()) {
      $this->getNextUrl($results);
    }
    return $results;
  }

  /**
   * Builds the next query url, or false if none.
   *
   * @param \Drupal\salesforce\SelectQueryResult $results
   */
  public function getNextUrl(SelectQueryResult $results) {
    if (empty($results->nextRecordsUrl())) {
      return FALSE;
    }
    $version_path = parse_url($this->sfApi->getApiEndPoint(), PHP_URL_PATH);
    return str_replace($version_path, '', $results->nextRecordsUrl());
  }

  /**
   * @param $nextRecordsUrl
   *
   * @return bool
   */
  public function getMore(SelectQueryResult $results) {
    try {
      return $this->sfApi->queryMore($results);
    } catch (\Exception $e) {
      // @todo provide more informative message
      watchdog_exception(LogLevel::NOTICE, $e);
    }
    return FALSE;
  }
}
