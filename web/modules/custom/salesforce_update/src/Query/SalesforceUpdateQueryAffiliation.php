<?php

namespace Drupal\salesforce_update\Query;

/**
 * @file
 * Contains the SalesforceUpdateQueryAffiliation class.
 */

/**
 * @class SalesforceUpdateQueryAffiliation
 * Manages and executes the Affiliation query to Salesforce
 * Affiliations are connections between accounts and opportunities
 * Used in conjuction with salesforceUpdateQueryOpportunity
 */
class SalesforceUpdateQueryAffiliation extends SalesforceUpdateQuery {

  //Type of salesforce object to get
  private $sfObj = "npe5__Affiliation__c";

  //Fields to retrieve from salesforce
  private $fields = [
    "npe5__Organization__r.Public_Name__c",
    "npe5__Organization__r.BillingCity",
    "npe5__Organization__r.BillingState",
    "npe5__Organization__r.BillingCountry",
    "npe5__Organization__r.CreativeGround_Profile_URL__c",
    "Primary_Artist_Request_Affiliation__c",
    "RoleforThisRequest__c",
    "Request__c",
    "id",
  ];

  //The roles we want
  private $allowedRoles = [
    "Artist/Ensemble",
    "Applicant",
    "Exchange Artist",
  ];


  /**
   * @param $requestIds : An array of Opportunity Ids from salesforce
   */
  public function __construct($requestIds) {
    switch (gettype($requestIds)) {
      case "string":
        $requestIds = [$requestIds];
        break;
      case "array":
        break;
      default:
        $msg = "Array of Salesforce Opportunity Ids required.";
        _salesforce_update_error($msg, NULL, __CLASS__, __METHOD__);
    }

    parent::__construct($this->sfObj);
    $this->addFields($this->fields);
    $this->addCondition("npe5__EndDate__c", "null");
    $this->addCondition("Request__c", $requestIds, "IN");
    $this->addCondition("RoleforThisRequest__c", $this->allowedRoles, "IN");
  }
}