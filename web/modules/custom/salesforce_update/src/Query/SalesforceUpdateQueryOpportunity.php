<?php

namespace Drupal\salesforce_update\Query;

/**
 * @class SalesforceUpdateQueryOpportunity
 * Manages and executes the Opportunity query to Salesforce
 * Opportunities are Donations/Requests
 */
class SalesforceUpdateQueryOpportunity extends SalesforceUpdateQuery {

  //Date of latest update
  private $lastUpdate;

  //Type of salesforce object to get
  private $sfObj = "Opportunity";

  //Fields to retrieve from salesforce
  private $fields = [
    "Id",
    "RecordTypeId",
    "RecordType.Name",
    "AdjGrantAmt__c",
    "Fiscal_Year_Text__c",
    "ProjTitle__c",
    "Summary__c",
    "DecisionAck__c",
  ];

  //Request statuses that we want to import
  private $allowedStatus = [
    "Awarded",
    "Selected for NDPTO",
  ];

  //Grants we don't want to pull
  private $badGrants = [
    "012d0000000SQ3rAAG",
    "012d0000000SyqSAAS",
    "012d0000000Rp4vAAC",
  ];

  public function __construct() {
    parent::__construct($this->sfObj);
    $this->lastUpdate = \Drupal::state()->get('salesforce_update.last');
    $this->addFields($this->fields);
    /**
     * @condition
     * We can't use the FiscalYear numeric field because it doesn't match
     * the client's Fiscal_Year_Text field. We have to do a string
     * comparison.
     */
    $this->addCondition('Fiscal_Year_Text__c', $this->getYearList(2013), 'IN');
    $this->addCondition("StageName", $this->allowedStatus, "IN");
    foreach ($this->badGrants as $grant) {
      $this->addCondition("RecordTypeId", $grant, "!=");
    }
  }

  /**
   * Get all years between now and $limit
   */
  private function getYearList($limit) {
    $years = [];
    $year = date("Y") + 5;
    while ($year >= $limit) {
      $years[] = $year--;
    }
    return $years;
  }

  /**
   * Runs the same base query, but sets the last-updated variable too
   */
  public function query() {
    return parent::query();
  }
}
