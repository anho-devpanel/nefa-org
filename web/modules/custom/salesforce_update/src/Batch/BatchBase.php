<?php

namespace Drupal\salesforce_update\Batch;

use Drupal\Core\StringTranslation\StringTranslationTrait;

class BatchBase {
  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempstore;

  public function __construct() {
    $this->tempstore = \Drupal::service('tempstore.private')->get('salesforce_update');
  }
}