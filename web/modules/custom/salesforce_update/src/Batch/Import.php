<?php

namespace Drupal\salesforce_update\Batch;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\salesforce_update\SObject;
use Drupal\salesforce_update\Query\SalesforceUpdateQueryAffiliation;
use Drupal\salesforce_update\Map\SalesforceUpdateImport;
use Drupal\salesforce_update\Map\SalesforceUpdateMap;
use Psr\Log\LogLevel;

class Import extends BatchBase {

  use StringTranslationTrait;

  /**
   * Maps and imports a single result into an entity
   */
  public function process($null = NULL, &$context) {
    $mapper = new SalesforceUpdateMap();
    $importer = new SalesforceUpdateImport();

    if (!isset($context['sandbox']['results'])) {
      $context['sandbox']['results'] = $this->tempstore->get('query_result');
      $this->strip_attributes($context['sandbox']['results']);
      $context['sandbox']['total'] = count($context['sandbox']['results']);
      $context['sandbox']['processed'] = 0;
    }

    if (!isset($context['results']['imported'])) {
      $context['results']['imported'] = 0;
      $context['results']['not_imported'] = 0;
    }

    $record = array_shift($context['sandbox']['results']);
    // The D7 version of the module required setting the value of a field, but
    // \Drupal\salesforce\SObject doesn't allow setting values, so we need to
    // rebuild all records using \Drupal\salesforce_update\SObject, a new class
    // we created to overcome this limitation. This also catches an
    $record = new SObject($record);

    $this->add_affiliations($record);
    $drupalValues = $mapper->translate($record);

    if (empty($drupalValues)) {
      $context['results']['not_imported']++;
    }
    else {
      //Importer returns 0 on success, 1 on error, 2 on shouldUpdate === false
      $eCode = $importer->import($drupalValues);
      switch ($eCode) {
        case 0:
          $context['results']['imported']++;
          $status_message = 'Importing %title (record @count of @total)';
          break;
        case 1:
        case 2:
          foreach ($importer->getMessages() as $message) {
            drupal_set_message($message, 'warning', FALSE);
          }

          $status_message = 'Unable to import id @id (record @count of @total). See warnings at the end of the process.';
          $context['results']['not_imported']++;
          break;
      }
    }


    $context['sandbox']['processed']++;
    $context['message'] = $this->t($status_message,
      [
        '%title' => $drupalValues['title'],
        '@count' => $context['sandbox']['processed'],
        '@total' => $context['sandbox']['total'],
        '@id' => $drupalValues['field_id'],
      ]
    );
    $context['finished'] = empty($context['sandbox']['results']) ?
      1 : $context['sandbox']['processed'] / $context['sandbox']['total'];
  }

  /**
   * Runs the affiliation query and adds the results to the opportunity record.
   *
   * @param \Drupal\salesforce_update\SObject $record
   *
   * @todo Modify to be saner
   */
  protected function add_affiliations(\Drupal\salesforce_update\SObject $record) {
    try {
      // This is to maintain the structure of the function as close as possible
      // to the D7 original, but there's room for improvement
      $sfIds = [$record->field('Id')];
    }
    catch (\Exception $e) {
      // @todo provide more informative message
      watchdog_exception(LogLevel::WARNING, $e);
      return;
    }

    $results = [];

    /**
     * Some of these queries can get too large for Salesforce. So we have to
     * split into two at a certain size.
     */
    if (count($sfIds) < 500) {
      $this->get_affiliations($sfIds, $results);
    }
    else {
      $split = array_chunk($sfIds, 500);
      foreach ($split as $array) {
        $this->get_affiliations($array, $results);
      }
    }
    //If we didn't find anything, stop
    if (empty($results)) {
      return;
    }

    foreach ($results as $affiliate) {
      $role = $affiliate->field('RoleforThisRequest__c') === "Applicant" ? "Applicant" : "Artist";

      try {
        if ($affiliate->field('Request__c') == $record->field('Id')
          // If there is not already data for this role, add this.
          // If there IS already data, only overwrite it with the affiliation marked
          // as "primary" for the role.
          && (empty($record->field($role)) || $affiliate->field('Primary_Artist_Request_Affiliation__c') == TRUE)) {
          // This is where the new SObject class comes in handy
          $record->set($role, $affiliate->field('npe5__Organization__r'));
        }
      } catch (\Exception $e) {
        // @todo provide more informative message
        watchdog_exception(LogLevel::NOTICE, $e);
      }
    }
  }


  /**
   * Retrieves affiliations from salesforce and appends to a result array
   *
   * @param array $sfIds An array of salesforce Ids to use in query
   * @param array $results The array to append our results to
   */
  protected function get_affiliations($sfIds, &$results) {
    $aq = new SalesforceUpdateQueryAffiliation($sfIds);
    $affiliations = $aq->query();
    if (!empty($affiliations) && !empty($affiliations->records())) {
      $results = array_merge($results, $affiliations->records());
    }
  }

  /**
   * Builds a map of $fieldValue => array key, for use in
   * $this->add_affiliations
   *
   * @param array $results The array of records
   * @param string $field The field to map on
   *
   * @return array $result[$field] => key of $result
   */
  protected function key_map($results, $field) {
    $map = [];
    foreach ($results as $key => $record) {
      $map[$record->field($field)] = $key;
    }
    return $map;
  }


  /**
   * Remove attribute arrays from records
   *
   * @param array $array The array with possible 'attributes' key
   */
  protected function strip_attributes(&$array) {
    unset($array['attributes']);
    foreach ($array as &$value) {
      if (is_array($value)) {
        $this->strip_attributes($value);
      }
    }
  }
}