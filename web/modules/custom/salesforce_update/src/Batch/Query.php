<?php

namespace Drupal\salesforce_update\Batch;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\salesforce_update\Query\SalesforceUpdateQueryOpportunity;

/**
 * Class Query
 *
 * @package Drupal\salesforce_update\Batch
 */
class Query extends BatchBase {

  use StringTranslationTrait;

  /**
   * The query filters.
   *
   * @var array
   */
  protected $filters;

  /**
   * Query constructor.
   */
  public function __construct() {
    parent::__construct();
    $this->filters = $this->tempstore->get('filters');
  }

  /**
   * Processes batch query.
   * @param $null
   * @param $context
   */
  public function process($null = NULL, &$context) {
    $query = new SalesforceUpdateQueryOpportunity();

    if (!isset($context['sandbox']['result'])) {
      $context['sandbox']['processed'] = 0;

      /**
       * @var \Drupal\salesforce\SelectQueryResult
       */
      $result = $query->query();
    }
    else {
      /**
       * @var \Drupal\salesforce\SelectQueryResult
       */
      $result = $query->getMore($context['sandbox']['result']);
    }

    if ($records = $result->records()) {
      $this->filter_query($records);

      if (!empty($records)) {
        $context['results'] += $records;
      }
    }

    $this->tempstore->set('query_result', $context['results']);
    $context['sandbox']['more'] = $query->getNextUrl($result);
    $context['sandbox']['result'] = $result;
    $context['sandbox']['processed'] += count($records);
    $context['message'] = $this->formatPlural(
      $context['sandbox']['processed'],
      'Retrieved 1 record from Salesforce.',
      'Retrieved @count records from Salesforce.'
    );
    $context['finished'] = empty($context['sandbox']['more']);
  }

  /**
   * Removes all records outside the decision date range
   * Client originally wanted to filter on fiscal year, later wanted to
   * filter on decision date.
   * The decision date field is a string, so we can't do numerical/date checking
   * in query, so we have to remove them here.
   *
   * @param $results array The results
   */
  protected function filter_query(&$results) {
    static $s_day = NULL;
    static $e_day = NULL;
    $filters = $this->filters;

    if (is_null($s_day)) {
      // Start date can be 0 if empty
      $s_day = empty($filters['start_date']) ? 0 : $this->date_integer($filters['start_date']);
    }

    if (is_null($e_day)) {
      // End date can't be infinite, so we need a trick
      $e_day = empty($filters['end_date']) ? FALSE : $this->date_integer($filters['end_date']);
    }

    foreach ($results as $key => $record) {
      if (empty($record->field('DecisionAck__c'))) {
        if (empty($filters['include_empty'])) {
          unset($results[$key]);
        }
        continue;
      }
      $d_day = $this->date_integer($record->field('DecisionAck__c'));

      // If record is before the start date OR
      // If there is an end date AND record is after the end date
      if ($d_day < $s_day || ($e_day !== FALSE && $d_day > $e_day)) {
        unset($results[$key]);
      }
    }
  }

  /**
   * Strips dashes from a date string.
   *
   * @param string $date
   */
  protected function date_integer($date) {
    return str_replace('-', '', $date);
  }
}