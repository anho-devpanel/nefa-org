<?php

namespace Drupal\salesforce_update\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\salesforce_update\SalesforceUpdate;

/**
 * Form constructor for Salesforce grant-recipient update form.
 */
class Update extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'salesforce_update_update';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [
      'update_container' => [
        '#type' => "container",

        'dates_container' => [
          '#type' => "container",

          'start_date' => [
            '#type' => 'date',
            '#title' => 'Start Date',
            '#date_format' => "Y-m-d",
            '#default_value' => \Drupal::state()->get('salesforce_update.last'),
          ],

          'end_date' => [
            '#type' => 'date',
            '#title' => 'End Date',
            '#date_format' => "Y-m-d",
          ],

          'include_empty' => [
            '#type' => 'checkbox',
            '#title' => t('Include empty dates'),
            '#description' => t('If checked, records that do not have a Decision Date Acknowledged will also be processed.'),
            '#default_value' => \Drupal::state()->get('salesforce_update.include_empty'),
          ],
        ],
      ],
      'actions' => [
        '#type' => 'actions',
        'submit' => [
          '#type' => 'submit',
          '#value' => t('Update grant recipients'),
          '#button_type' => 'primary',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $filters['start_date'] = $form_state->getValue('start_date');

    if ($end_date = $form_state->getValue('end_date')) {
      $filters['end_date'] = $end_date;

      \Drupal::state()->set('salesforce_update.last', $filters['end_date']);
    }

    $filters['include_empty'] = $form_state->getValue('include_empty');
    \Drupal::state()->set('salesforce_update.include_empty', $filters['include_empty']);

    $update = new SalesforceUpdate($filters);
    $update->update();
  }
}