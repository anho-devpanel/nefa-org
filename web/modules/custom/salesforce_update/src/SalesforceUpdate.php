<?php

namespace Drupal\salesforce_update;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\salesforce_update\Batch\Import;
use Drupal\salesforce_update\Batch\Query;

class SalesforceUpdate {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempstore;

  /**
   * SalesforceUpdate constructor.
   *
   * @param array $filters
   */
  public function __construct(array $filters) {
    $this->tempstore = \Drupal::service('tempstore.private')
      ->get('salesforce_update');
    $this->tempstore->set('filters', $filters);
    $this->tempstore->set('query_result', []);
  }

  /**
   * Retrieves records from Salesforce and creates batch jobs to import them
   * into Drupal.
   */
  public function update() {

    $query = new Query();
    $import = new Import();

    $batch = [
      'title' => "Importing Salesforce records",
      'operations' => [
        [
          [$query, 'process'],
          [NULL],
        ],
        [
          [$import, 'process'],
          [NULL],
        ],
      ],
      'init_message' => $this->t('Getting started…'),
      'progress_message' => $this->t('Making progress…'),
      'finished' => [$this, 'finished'],
    ];

    batch_set($batch);
  }

  /**
   * Batch API finishing callback
   *
   * @param boolean $success
   *   Whether the batch finished successfully.
   * @param array $results
   *   Detailed informations about the result.
   * @param array $operations
   *   Any leftover operations (from errors)
   */
  public function finished($success, $results, $operations) {
    if ($success) {
      drupal_set_message(
        $this->formatPlural($results['imported'], 'Successfully imported 1 Salesforce record.', 'Successfully imported @count Salesforce records.')
      );
      
      if (!empty($results['not_imported'])) {
        drupal_set_message(
          $this->formatPlural($results['not_imported'], '1 Salesforce record could not me imported.', '@count Salesforce records could not be imported.'), 'warning'
        );
      }
    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $message = t('An error occurred while processing %error_operation with arguments: @arguments', [
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE),
      ]);
      drupal_set_message($message, 'error');
    }
  }
}