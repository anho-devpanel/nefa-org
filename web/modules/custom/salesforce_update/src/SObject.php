<?php

namespace Drupal\salesforce_update;

use Drupal\salesforce\SObject as SObjectBase;
use Psr\Log\LogLevel;

/**
 * Extends SObject to allow setting field values.
 */
class SObject extends SObjectBase {

  /**
   * SObject constructor.
   *
   * @param \Drupal\salesforce\SObject $object
   */
  public function __construct(SObjectBase $object) {
    $this->type = $object->type();
    $this->id = $object->id();
    $this->fields = $object->fields();
  }

  /**
   * Given $key, return corresponding field value.
   *
   * Overriding SObjectBase::field() to prevent throwing an exception, which
   * is frankly a crappy way to handle this.
   *
   * @return mixed
   */
  public function field($key) {
    if (!array_key_exists($key, $this->fields)) {
      return NULL;
    }

    try {
      return parent::field($key);
    } catch (\Exception $e) {
      // There should be no need to handle this more elegantly, since
      // the exception should have been prevented by the if statement above
      watchdog_exception(LogLevel::NOTICE, $e);
    }
  }

  /**
   * Sets field values.
   *
   * @param $key
   * @param $value
   */
  public function set($key, $value) {
    $this->fields[$key] = $value;
  }
}