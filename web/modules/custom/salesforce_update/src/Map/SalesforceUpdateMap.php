<?php

namespace Drupal\salesforce_update\Map;

use Drupal\salesforce\SObject;

/**
 * @class SalesforceUpdateMapper
 * Maps Salesforce fields to Drupal Node Properties
 */
class SalesforceUpdateMap {

  //An array of SalesforceUpdateField objects
  public $mapping = [];

  //Salesforce baseURl, for use in addNotice
  private $sfUrl = "https://na14.salesforce.com/";


  public function __construct() {
    $this->createMaps();
  }

  /**
   * Look here for field map settings
   */
  private function createMaps() {
    //See SalesforceUpdateField for explanation of arrays
    $id = [
      "drupalField" => "field_id",
      "sfFields" => ["Id"],
    ];
    $this->mapping[] = new SalesforceUpdateField($id);

    $title = [
      "drupalField" => "title",
      "sfFields" => ["Applicant.Public_Name__c"],
    ];
    $this->mapping[] = new SalesforceUpdateField($title);

    $grant = [
      "drupalField" => "field_grant_or_program",
      "sfFields" => ["RecordType.Name", "RecordTypeId"],
      "type" => "complex",
      "callback" => [$this, "getGrant"],
    ];
    $this->mapping[] = new SalesforceUpdateField($grant);

    $amount = [
      "drupalField" => "field_grant_amount",
      "sfFields" => ["AdjGrantAmt__c"],
      "type" => "complex",
      "callback" => [$this, "fixAmount"],
    ];
    $this->mapping[] = new SalesforceUpdateField($amount);

    $year = [
      "drupalField" => "field_fiscal_year_awarded",
      "sfFields" => ["Fiscal_Year_Text__c"],
    ];
    $this->mapping[] = new SalesforceUpdateField($year);

    $city = [
      "drupalField" => "field_city",
      "sfFields" => ["Applicant.BillingCity"],
    ];
    $this->mapping[] = new SalesforceUpdateField($city);

    $state = [
      "drupalField" => "field_state",
      "sfFields" => ["Applicant.BillingState"],
      "type" => "complex",
      "callback" => [$this, "fixAppStates"],
    ];
    $this->mapping[] = new SalesforceUpdateField($state);

    $country = [
      "drupalField" => "field_country",
      "sfFields" => ["Applicant.BillingCountry"],
      "type" => "complex",
      "callback" => [$this, "fixApplicantCountry"],
    ];
    $this->mapping[] = new SalesforceUpdateField($country);

    $project = [
      "drupalField" => "field_project_title",
      "sfFields" => ["ProjTitle__c"],
    ];
    $this->mapping[] = new SalesforceUpdateField($project);

    $summary = [
      "drupalField" => "body",
      "sfFields" => ["Summary__c"],
    ];
    $this->mapping[] = new SalesforceUpdateField($summary);

    $artName = [
      "drupalField" => "field_artist_name",
      "sfFields" => ["Artist.Public_Name__c"],
    ];
    $this->mapping[] = new SalesforceUpdateField($artName);

    $artCity = [
      "drupalField" => "field_artist_city",
      "sfFields" => ["Artist.BillingCity"],
    ];
    $this->mapping[] = new SalesforceUpdateField($artCity);

    $artState = [
      "drupalField" => "field_artist_state",
      "sfFields" => ["Artist.BillingState"],
      "type" => "complex",
      "callback" => [$this, "fixArtStates"],
    ];
    $this->mapping[] = new SalesforceUpdateField($artState);

    $artCountry = [
      "drupalField" => "field_artist_country",
      "sfFields" => ["Artist.BillingCountry"],
      "type" => "complex",
      "callback" => [$this, "fixArtistCountry"],
    ];
    $this->mapping[] = new SalesforceUpdateField($artCountry);

    $decisionDate = [
      "drupalField" => "field_decision_date",
      "sfFields" => ["DecisionAck__c"],
    ];
    $this->mapping[] = new SalesforceUpdateField($decisionDate);

    $creativeGroundUrl = [
      "drupalField" => "field_creativeground_profile_url",
      "sfFields" => ["Artist.CreativeGround_Profile_URL_c"],
      "type" => "complex",
      "callback" => [$this, "buildCreativeGroundUrl"],
    ];
    $this->mapping[] = new SalesforceUpdateField($creativeGroundUrl);
  }

  /**
   * Translates sf records into drupal records
   *
   * @param \Drupal\salesforce\SObject $record
   *
   * @return array
   */
  public function translate(SObject $record) {
    $output = [];
    //Run a quick check to see how everything is.
    $this->addNotice($record, $output);
    foreach ($this->mapping as $fieldMap) {
      $fieldName = $fieldMap->getDrupalField();
      $value = $fieldMap->translate($record);
      $output[$fieldName] = $value;
    }
    return $output;
  }


  /* ----- Callbacks ------------------------ */

  /**
   * Determining the "Grant or Project" field
   *
   * @param \Drupal\salesforce\SObject $record
   *
   * @return mixed
   * @throws \Exception
   */
  public function getGrant(SObject $record) {
    static $grants = [];

    $rId = $record->field('RecordTypeId');

    if (!isset($grants[$rId])) {
      $eQuery = \Drupal::entityQuery('node')
        ->condition('type', 'grant')
        ->condition('field_salesforce_grant_rec_ids.value', $rId);
      $results = $eQuery->execute();

      if (empty($results)) {
        $name = $record->field('RecordType')['Name'];
        drupal_set_message(t("Could not find %grant_name in the system. Add the Salesforce ID <strong>@grant_id</strong> to the corresponding grant node, then import records again.", ['%grant_name' => $name, '@grant_id' => $rId]), 'warning', FALSE);
        $grants[$rId] = FALSE;
      }
      else {
        //We just need the first result
        $grants[$rId] = reset($results);
      }
    }

    return $grants[$rId];
  }

  /**
   * Determining the Amount field
   */
  public function fixAmount($record) {
    $amount = $record->field('AdjGrantAmt__c');
    if (gettype($amount) !== "string") {
      $amount = "{$amount}";
    }
    //Remove and store change, if it's there
    if (strpos($amount, '.') !== FALSE) {
      $numParts = explode('.', $amount);
      if (isset($numParts[1]) && $numParts[1] != 0) {
        //Change is exactly two digits
        $change = substr($numParts[1], 0, 2);
        $change = str_pad($change, 2, "0");
      }
      $amount = $numParts[0];
    }
    //Remove non-numbers
    $amount = preg_replace('/\D/', '', $amount);
    //Remove leading 0's
    while ($amount !== "" && $amount[0] == "0") {
      $amount = substr($amount, 1);
    }
    //Add d-sign and commas
    $amount = empty($amount) ? "" : "$" . number_format($amount);
    //Return number (with change if necessary)
    return isset($change) ? $amount . "." . $change : $amount;
  }

  /**
   * Removes 'XX' from applicant-state fields
   */
  public function fixAppStates($record) {
    if (empty($record->field('Applicant')) ||
      $record->field('Applicant')['BillingState'] === "XX") {
      return "";
    }
    return $record->field('Applicant')['BillingState'];
  }

  /**
   * Removes 'XX' from artist-state fields
   */
  public function fixArtStates($record) {
    if (empty($record->field('Artist')) ||
      $record->field('Artist')['BillingState'] === "XX") {
      return "";
    }
    return $record->field('Artist')['BillingState'];
  }

  /**
   * Determining the country field
   */
  public function fixApplicantCountry($record) {
    if (empty($record->field('Applicant')['BillingCountry'])) {
      return "";
    }
    return $this->standardizeUnitedStates($record->field('Applicant')['BillingCountry']);
  }

  public function fixArtistCountry($record) {
    if (empty($record->field('Artist')['BillingCountry'])) {
      return "";
    }
    return $this->standardizeUnitedStates($record->field('Artist')['BillingCountry']);
  }

  /**
   * Builds the url array to pass to Drupal's link module.
   *
   * @param \Drupal\salesforce\SObject $record
   *
   * @return array
   * @throws \Exception
   */
  public function buildCreativeGroundUrl(SObject $record) {
    if (empty($record->field('Artist'))
      || empty($record->field('Artist')['CreativeGround_Profile_URL__c'])) {
      return [];
    }

    $value = $record->field('Artist')['CreativeGround_Profile_URL__c'];

    if (!preg_match("/^https?:\/\//", $value)) {
      $value = "http://$value";
    }

    return ['uri' => $value];
  }

  /**
   * Checks the record for errors and builds a notice array if necessary
   * The notice array will be used after import to generate a useful error
   *
   * @param \Drupal\salesforce\SObject $record
   * @param $output
   */
  private function addNotice(SObject $record, &$output) {
    $msgs = $this->checkRecord($record);
    if (!empty($msgs)) {
      $sfUrl = $this->buildSfUrl($record->field('Id'));
      $output['notice'] = [
        'sfUrl' => $sfUrl,
        'msgs' => $msgs,
      ];
    }
  }

  /**
   * Builds a url to a Salesforce opportunity, using the unique ID
   *
   * @param string $id The salesforce id
   *
   * @return string The salesforce url
   */
  private function buildSfUrl($id) {
    return $this->sfUrl . substr($id, 0, -3);
  }

  /**
   * Checking the fields, to alert NEFA if they're missing something
   *
   * @param \Drupal\salesforce\SObject $record
   *
   * @return array
   * @throws \Exception
   */
  private function checkRecord(SObject $record) {
    $messages = [];

    //Grant Recipient
    if (empty($record->field('Applicant')) ||
      empty($record->field('Applicant')['Public_Name__c'])) {
      $messages[] = t('The record with id %id does not have a recipient and will not be saved.', ['%id' => $record->field('Id')]);
      $name = "unknown (" . $record->field('Id') . ")";
    }
    else {
      $name = "{$record->field('Applicant')['Public_Name__c']} ({$record->field('Id')})";
    }

    $placeholders = ['%name' => $name];

    //Recipient City
    if (empty($record->field('Applicant')['BillingCity'])) {
      $messages[] = t('Record for %name does not have a City.', $placeholders);
    }

    //Recipient State OR Country
    if (empty($record->field('Applicant')['BillingState']) &&
      empty($record->field('Applicant')['BillingCountry'])) {
      $messages[] = t('Record for %name does not have a State/Country.', $placeholders);
    }

    //Fiscal Year
    if (empty($record->field('Fiscal_Year_Text__c'))) {
      $messages[] = t('Record for %name does not have a Fiscal Year.', $placeholders);
    }

    //Summary
    if (empty($record->field('Summary__c'))) {
      $messages[] = t('Record for %name does not have a Summary.', $placeholders);
    }

    //Amount
    if (empty($record->field('AdjGrantAmt__c'))) {
      $messages[] = t('Record for %name does not have an Adjusted Grant Amount.', $placeholders);
    }

    return $messages;
  }

  /**
   * Standardizes the way the USA is represented in the country field
   *
   * @param string $country The Country name from SF
   *
   * @return string The country name
   *
   * @todo Change to using regex
   */
  private function standardizeUnitedStates($country) {
    if (empty($country)) {
      return "";
    }
    $allUS = [
      "us",
      "u.s.",
      "u.s",
      "usa",
      "u.s.a.",
      "u.s.a",
      "united states",
      "the united states",
      "united states of america",
      "the united states of america",
    ];
    return in_array(strtolower($country), $allUS) ? "United States" : $country;
  }
}