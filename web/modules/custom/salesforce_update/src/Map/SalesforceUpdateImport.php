<?php

namespace Drupal\salesforce_update\Map;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\node\Entity\Node;
use Psr\Log\LogLevel;

/**
 * @class SalesforceUpdateImport
 * Maps Salesforce fields to Drupal Node Properties.
 */
class SalesforceUpdateImport {

  /**
   * Target bundle name.
   *
   * @var string
   */
  private $targetBundle;

  /**
   * The record to insert/update.
   *
   * @var array
   */
  private $importRecord = [];

  /**
   * The Salesforce Importer user
   *
   * @var int
   *
   * @todo allow setting this in configuration
   */
  private $sfImporterUid;

  /**
   * Any messages generated during the process.
   *
   * @var array
   */
  private $notice = [];

  /**
   * SalesforceUpdateImport constructor.
   */
  public function __construct() {
    $this->targetBundle = 'grant_recipient';
    $this->sfImporterUid = 5;
  }

  /**
   * Save imported record to a node.
   *
   * @param array $importRecord
   *
   * @return int
   */
  public function import(array $importRecord) {
    if (!is_array($importRecord)) {
      $msg = "Parameter does not exist or is not an array.";
      _salesforce_update_error($msg, $importRecord, __CLASS__, __METHOD__);
      return 1;
    }
    $this->importRecord = $importRecord;
    $this->getNotice(); //Moves the notice from import to property

    // The node title is mandatory, so we can proceed only if that exists
    if (empty($importRecord['title'])) {
      return 1;
    }

    $alreadyExists = $this->alreadyExists($this->importRecord['field_id']);
    if ($alreadyExists === FALSE) {
      $entity = Node::create(
        [
          'type' => $this->targetBundle,
          'status' => 0,
          'uid' => $this->sfImporterUid,
        ] + $this->importRecord
      );
    }
    else {
      $entity = Node::load($alreadyExists);
      // field_update actually means DO NOT UPDATE, so do nothing if it's set.
      // This is the quickest way to get a usable value
      if (!empty($entity->get('field_update')->getString())) {
        return 2;
      }

      $this->insertFields($entity);
    }

    try {
      $entity->save();
    } catch (EntityStorageException $e) {
      watchdog_exception(LogLevel::WARNING, $e);
      drupal_set_message(t('%title could not be saved.', ['%title' => $entity->getTitle()]), 'warning', FALSE);
      $return = 1;
    }

    //Reset the importRecord
    $this->importRecord = [];
    return isset($return) ? $return : 0;
  }

  /**
   * Checks if an entity with the appropriate field_id (from sf) already exists.
   *
   * @param string $id
   *
   * @return bool|mixed
   */
  private function alreadyExists($id) {
    $eQuery = \Drupal::entityQuery('node')
      ->condition('type', $this->targetBundle)
      ->condition('field_id.value', $id);
    $results = $eQuery->execute();
    if (empty($results)) {
      return FALSE;
    }
    else {
      //We just need the first result
      return reset($results);
    }
  }

  /**
   * @param \Drupal\node\Entity\Node $record
   *
   * @return bool|\Drupal\node\Entity\Node
   */
  private function insertFields(Node $record) {
    if (!is_object($record)) {
      $msg = "Cannot insert fields into a non-object.";
      _salesforce_update_error($msg, $record, __CLASS__, __METHOD__);
      return FALSE;
    }
    foreach ($this->importRecord as $fieldName => $fieldValue) {
      $record->set($fieldName, $fieldValue);
    }
  }

  /**
   * Retrieves any notices applied to the import record and removes them
   * from import array.
   */
  private function getNotice() {
    if (!empty($this->importRecord['notice'])) {
      $this->notice = $this->importRecord['notice'];
    }
    unset($this->importRecord['notice']);
  }

  /**
   * Returns the Salesforce Importer UID.
   */
  public function getSfUID() {
    return $this->sfImporterUid;
  }

  /**
   * Returns an array of messages from the importer notice.
   *
   * @return array
   */
  public function getMessages() {
    return !empty($this->notice['msgs']) ? $this->notice['msgs'] : [];
  }
}
