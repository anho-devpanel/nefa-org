<?php

namespace Drupal\salesforce_update\Map;

/**
 * @file
 * Contains the SalesforceUpdateField class.
 */

use Drupal\salesforce\SObject;
use Psr\Log\LogLevel;

/**
 * @class SalesforceUpdateField
 * Maps a single drupal field from one or more salesforce fields
 */
class SalesforceUpdateField {

  //Drupal field - can only be one field.
  private $drupalField = "";

  //The salesforce field(s)
  private $sfFields = [];

  //Basic fields are a straight 1->1 map.
  //Complex fields must provide a callback
  private $type = "basic";

  //Callback for complex fields
  private $callback = "";

  /**
   * The constructor, sets all the properties
   *
   * @param array $options
   *   drupalField => "single field" (required)
   *   sfFields => array("one or", "more salesforce", "fields") (required)
   *   type => "basic" || "complex" (default: "basic")
   *   callback => callable function (required for complex types)
   */
  public function __construct($options) {
    //All the checks
    if (!$this->checkOptions($options)) {
      return FALSE;
    }

    $this->drupalField = $options['drupalField'];
    $this->sfFields = $options['sfFields'];
    if (isset($options['type']) && $options['type'] === "complex") {
      $this->type = "complex";
      $this->callback = $options['callback'];
    }
  }

  /**
   * Determines the value for the field.
   *
   * @param \Drupal\salesforce\SObject $record
   *
   * @return mixed|string
   */
  public function translate(SObject $record) {
    //Basic fields are 1-to-1 mapping. Pretty basic!
    if ($this->type === "basic") {
      $targetValue = $this->getSfValue($this->sfFields[0], $record);
      return is_null($targetValue) ? "" : $targetValue; //NULL becomes ""
    }

    //Complex fields come with a callback array
    return call_user_func($this->callback, $record);
  }

  /**
   * Retrieves the appropriate value from the record array/sub-arrays
   *
   * @param string $fieldName THe field name(s) to get.
   * @param \Drupal\salesforce\SObject $record The record
   *
   * @return string The value to import
   */
  private function getSfValue($fieldName, SObject $record) {
    $fieldParts = explode('.', $fieldName);
    switch (count($fieldParts)) {
      case 1:
        try {
          return !empty($record->field($fieldParts[0])) ? $record->field($fieldParts[0]) : "";
        } catch (\Exception $e) {
          watchdog_exception(LogLevel::NOTICE, $e);
        }
        break;
      case 2:
        //Some results are inside a sub array
        //Ex: Applicant.Public_Name --> $record['Applicant"][Public_Name"]
        try {
          return !empty($record->field($fieldParts[0])[$fieldParts[1]]) ? $record->field($fieldParts[0])[$fieldParts[1]] : "";
        } catch (\Exception $e) {
          watchdog_exception(LogLevel::NOTICE, $e);
        }
        break;
      default:
        return '';
    }
  }

  /**
   * Does a billion checks on the options array
   *
   * @param array $options The options being passed to the class constructor
   *
   * @return boolean
   */
  private function checkOptions($options) {
    if (!is_array($options)) {
      $msg = "Options argument is not an array.";
    }
    elseif (empty($options['drupalField']) || !is_string($options['drupalField'])) {
      $msg = "No 'drupalField' string in options";
    }
    elseif (empty($options['sfFields']) || !is_array($options['sfFields'])) {
      $msg = "No 'sfFields' array in options.";
    }
    elseif (isset($options['type']) && $options['type'] === "complex" && (empty($options['callback']) || !is_callable($options['callback']))) {
      $msg = "Complex maps must have 'callback' option defined and callable";
    }
    if (isset($msg)) {
      _salesforce_update_error($msg, $options, __CLASS__, "__construct");
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Allows us to access the drupalField property, without making it public.
   *
   * @return string The drupal field name
   */
  public function getDrupalField() {
    return $this->drupalField;
  }
}