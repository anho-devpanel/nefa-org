<?php
/**
 * @file
 * Contains \Drupal\custom_ds_fields\Plugin\DsField\NewsAuthorTitleLink.
 */

namespace Drupal\project_ds_code_fields\Plugin\DsField;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\file\Entity\File;

/**
 * Plugin that renders the image caption for small image banners.
 *
 * @DsField(
 *   id = "small_image_caption",
 *   title = @Translation("DS: Image caption for small image banners"),
 *   entity_type = "node",
 *   provider = "project_ds_code_fields",
 *   ui_limit = {"*|banner"}
 * )
 */
class SmallImageCaption extends DsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Fetch the entity
    $entity = $this->entity();
    $banner_type = "";
    $banner_type_landing = "";
    $caption = "";
    $render_array = [];

    // There are two different versions of a "banner type" field but both include an
    // option for "small-image"

    // If Banner Type (Landing) field exists
    if ($entity->hasField('field_banner_type_landing')) {
      $banner_type_landing = $entity->field_banner_type_landing->value;
    }
    // If Banner Type field exists
    if ($entity->hasField('field_banner_type')) {
      $banner_type = $entity->field_banner_type->value;
    }

    // Image captions are only relevant if it is a small image banner
    if (($banner_type_landing) && ($banner_type_landing == 'small-image') ||
      ($banner_type) && ($banner_type == 'small-image')) {

      // If an image caption exists
      if ($entity->hasField('field_image_caption')) {
        $caption = $entity->field_image_caption->value;
        if ($caption) {
          $render_array = ['#markup' => $caption];
        }
      }
    }

    return $render_array;

  }
}
