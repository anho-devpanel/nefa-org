<?php
/**
 * @file
 * Contains \Drupal\custom_ds_fields\Plugin\DsField\ArtistNameUnique.
 */

namespace Drupal\project_ds_code_fields\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\file\Entity\File;

/**
 * Plugin that renders the artist's name if it is different from grant recipient name
 *
 * @DsField(
 *   id = "grantee_artist_name",
 *   title = @Translation("DS: Artist Name not same as Recipient Name"),
 *   entity_type = "node",
 *   provider = "project_ds_code_fields",
 *   ui_limit = {"grant_recipient|*"}
 * )
 */
class ArtistNameUnique extends DsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Fetch the entity
    $entity = $this->entity();
    $recipient = $entity->label();
    $artist = "";
    $render_array = [];

    // If artist field exists
    if ($entity->hasField('field_artist_name')) {
      $artist = $entity->field_artist_name->value;
      if ($artist) {
        if ($artist != $recipient) {
          $render_array = ['#markup' => "<span class='artist-name-label'>Artist:</span> " . $artist];
        }
      }
    }

    return $render_array;

  }
}
