<?php
/**
 * @file
 * Contains \Drupal\project_ds_code_fields\Plugin\DsField\PersonGrayIcon.
 */

namespace Drupal\project_ds_code_fields\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;

/**
 * Plugin that renders a default person image if a photo does not exist.
 *
 * @DsField(
 *   id = "person_gray_icon",
 *   title = @Translation("DS: Person Gray Icon"),
 *   entity_type = "node",
 *   provider = "project_ds_code_fields",
 *   ui_limit = {"news_author|*"}
 * )
 */
class PersonGrayIcon extends DsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Fetch the entity
    $entity = $this->entity();
    $photo = "";
    $img_path = drupal_get_path('theme', 'nefa') . '/static/img/person-gray-icon.jpg';
    $default_image = '<img src="/' .$img_path .'" alt="">';
    $render_array = [];

    // If the main image field is empty, supply a default image with gray person icon against white background
    if (($entity->hasField('field_main_image')) && ($entity->get('field_main_image')->isEmpty()) ) {
      $render_array = [
        '#markup' => $default_image,
      ];
    }
    return $render_array;
  }

}
