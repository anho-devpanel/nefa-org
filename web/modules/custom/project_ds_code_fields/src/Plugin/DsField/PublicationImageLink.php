<?php
/**
 * @file
 * Contains \Drupal\custom_ds_fields\Plugin\DsField\PublicationImageLink.
 */

namespace Drupal\project_ds_code_fields\Plugin\DsField;

use Drupal\Core\Url;
use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\file\Entity\File;

/**
 * Plugin that renders Publication images linked to the publication file (pdf).
 *
 * @DsField(
 *   id = "publication_image_link",
 *   title = @Translation("DS: Publication Image Link"),
 *   entity_type = "node",
 *   provider = "project_ds_code_fields",
 *   ui_limit = {"publication|*"}
 * )
 */
class PublicationImageLink extends DsFieldBase {

  /**
   * {@inheritdoc}
   */

  public function build() {
    // Fetch the entity
    $entity = $this->entity();
    // If Publication file exists

    if ($entity->hasField('field_publication_file')) {
      // the file field on the referenced media entity
      $file_field = $entity->field_publication_file->entity->field_document;
      //if file field has value

      if ($file_field) {
        // grab the id of the file field instance
        $file_tid = File::load($file_field->target_id);
        // using the id, grab the file path of the field and convert into absolute url
        $file_url = file_create_url($file_tid->getFileUri());
        // And then if the Publication image field exists, link the image to the file
        //reference: https://drupal.stackexchange.com/questions/255010/render-image-that-is-linked

        if ($entity->hasField('field_main_image')) {
          // the image field on the referenced media entity
          $image_field = $entity->field_main_image->entity->field_image;
          //if image field has value

          if ($image_field) {
            // grab the id of the image field instance
            $image_tid = File::load($image_field->target_id);
            // using the id, grab the file path of the field.
            $image_uri = $image_tid->getFileUri();

            return [
              // render the array as a link
              '#type' => 'link',
              // the link is wrapped around a title property which is a nested render array, that render the image
              '#title' => [
                '#theme' => 'image_style',
                //image style name
                '#style_name' => 'publication',
                // render image from this path
                '#uri' => $image_uri,
              ],
              //render file path url
              '#url' => Url::fromUri($file_url),
            ];
          }
        }
      }
    }
    return [];
  }
}
