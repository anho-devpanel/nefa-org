<?php
/**
 * @file
 * Contains \Drupal\custom_ds_fields\Plugin\DsField\NewsAuthorTitleLink.
 */

namespace Drupal\project_ds_code_fields\Plugin\DsField;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\file\Entity\File;

/**
 * Plugin that renders the image caption for full image banners.
 *
 * @DsField(
 *   id = "full_image_caption",
 *   title = @Translation("DS: Image caption for full image banners"),
 *   entity_type = "node",
 *   provider = "project_ds_code_fields",
 *   ui_limit = {"*|full"}
 * )
 */
class FullImageCaption extends DsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Fetch the entity
    $entity = $this->entity();
    $banner_type = "";
    $banner_type_landing = "";
    $caption = "";
    $render_array = [];

    // For content types that are landing pages (Page nodes), there is a banner type option
    // of full image

    // If Banner Type (Landing) field exists
    if ($entity->hasField('field_banner_type_landing')) {
      $banner_type_landing = $entity->field_banner_type_landing->value;

      // Captions are only relevant if it is an image banner
      if (($banner_type_landing) && ($banner_type_landing == 'full-image')) {

        // If an image caption exists
        if ($entity->hasField('field_image_caption')) {
          $caption = $entity->field_image_caption->value;
          if ($caption) {
            $render_array = ['#markup' => "Photo Caption: " . $caption];
          }
        }
      }
    }

    return $render_array;

  }
}
