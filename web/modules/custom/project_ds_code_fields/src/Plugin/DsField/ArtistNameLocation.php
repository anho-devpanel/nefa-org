<?php
/**
 * @file
 * Contains \Drupal\custom_ds_fields\Plugin\DsField\NewsAuthorTitleLink.
 */

namespace Drupal\project_ds_code_fields\Plugin\DsField;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\file\Entity\File;

/**
 * Plugin that renders the artist's name and location
 *
 * @DsField(
 *   id = "grantee_name_location",
 *   title = @Translation("Artist"),
 *   entity_type = "node",
 *   provider = "project_ds_code_fields",
 *   ui_limit = {"grant_recipient|*"}
 * )
 */
class ArtistNameLocation extends DsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Fetch the entity
    $entity = $this->entity();
    $artist = "";
    $city = "";
    $state = "";
    $artist_info = [];
    $render_array = [];

    // If artist field exists
    if ($entity->hasField('field_artist_name')) {
      $artist = $entity->field_artist_name->value;
      if ($artist) {
        $artist_info[] = $artist;

        // If city field exists, add that too
        if ($entity->hasField('field_artist_city')) {
          $city = $entity->field_artist_city->value;
          if ($city) {
            $artist_info[] = $city;
          }
        }
        // If state field exists, add that too
        if ($entity->hasField('field_artist_state')) {
          $state = $entity->field_artist_state->value;
          if ($state) {
            $artist_info[] = $state;
          }
        }

        $artist_info = implode(", ", $artist_info);
        $render_array = ['#markup' => $artist_info];
      }
    }

    return $render_array;

  }
}
