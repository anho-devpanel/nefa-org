<?php
/**
 * @file
 * Contains \Drupal\custom_ds_fields\Plugin\DsField\BodTitle.
 */

namespace Drupal\project_ds_code_fields\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;

/**
 * Plugin that renders the Board of Directors role label preceded by the title if one exists.
 *
 * @DsField(
 *   id = "bod_title",
 *   title = @Translation("DS: BOD Title"),
 *   entity_type = "node",
 *   provider = "project_ds_code_fields",
 *   ui_limit = {"news_author|*"}
 * )
 */
class BodTitle extends DsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Fetch the entity
    $entity = $this->entity();
    $role_name = "";
    $title = "";
    $display_title = "";
    $render_array = [];

    // If role field exists
    if ($entity->hasField('field_roles')) {
      $field = $entity->field_roles;
      if ($field->value) {
        $role = $entity->field_roles[0]->value;
        // and its value is Board of Directors
        if ($role == 'bod') {
          // get the label
          $role_name = $field->getFieldDefinition()
            ->getFieldStorageDefinition()
            ->getOptionsProvider('value', $field->getEntity())
            ->getPossibleOptions()[$field->value];
          // if the Board of Director title has been set as well
          if ($entity->hasField('field_board_of_directors_title')) {
            $title = $entity->field_board_of_directors_title->value;
            // display the title before the role naem
            if ($title) {
              $display_title = $title . ", " . $role_name;
              // or just the role name if no title exists
            } else {
              $display_title = $role_name;
            }
          }
        }
      }
    }

    $render_array = ['#markup' => $display_title];

    return $render_array;

  }
}
