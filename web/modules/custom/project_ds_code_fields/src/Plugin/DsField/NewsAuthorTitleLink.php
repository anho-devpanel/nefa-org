<?php
/**
 * @file
 * Contains \Drupal\custom_ds_fields\Plugin\DsField\NewsAuthorTitleLink.
 */

namespace Drupal\project_ds_code_fields\Plugin\DsField;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\ds\Plugin\DsField\DsFieldBase;

/**
 * Plugin that renders News Author Title linked to a view of related news .
 *
 * @DsField(
 *   id = "news_author_title_link",
 *   title = @Translation("DS: News Author Title Link"),
 *   entity_type = "node",
 *   provider = "project_ds_code_fields",
 *   ui_limit = {"news_author|*"}
 * )
 */
class NewsAuthorTitleLink extends DsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Fetch the entity
    $entity = $this->entity();
    $news_view_nid = '10385'; // nid of the node with the News view we need to filter
    $id = $entity->id(); // nid of the News Author node which is used to filter the News view (this entity)
    $title = $entity->label(); // Title of the News Author node (this entity)
    $filter = "news-author-filter[" . $id . "]"; // the filter identifier in the News view
    $url = 'internal:/node/' . $news_view_nid;
    $linked_title = "";
    $render_array = [];
    $options = [
      'query' => [$filter => $id],
    ];


    $linked_title = Link::fromTextAndUrl(t('By ' . $title), Url::fromUri($url, $options))->toString();

    $render_array = ['#markup' => $linked_title];

    return $render_array;

  }
}
