<?php
/**
 * @file
 * Contains \Drupal\custom_ds_fields\Plugin\DsField\NewsAuthorTitleLink.
 */

namespace Drupal\project_ds_code_fields\Plugin\DsField;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\file\Entity\File;

/**
 * Plugin that renders the full width banner image.
 *
 * @DsField(
 *   id = "full_image",
 *   title = @Translation("DS: Full width banner image"),
 *   entity_type = "node",
 *   provider = "project_ds_code_fields",
 *   ui_limit = {"page|banner"}
 * )
 */
class FullImage extends DsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Fetch the entity
    $entity = $this->entity();
    $banner_type = "";
    $banner_type_landing = "";
    $image = "";
    $render_array = [];

    // For content types that are landing pages (Page nodes), there is a banner type option
    // of full image or small image

    // If Banner Type (Landing) field exists
    if ($entity->hasField('field_banner_type_landing')) {
      $banner_type_landing = $entity->field_banner_type_landing->value;

      // Images are only relevant if it is an image banner
      if (($banner_type_landing) && ($banner_type_landing == 'full-image')) {

        // If an image exists
        if (($entity->hasField('field_main_image') && (!$entity->get('field_main_image')->isEmpty()))) {
          $image = $entity->field_main_image->entity->field_image;
          if ($image) {
            $file = File::load($image->target_id);
            $url = $file->getFileUri();
            $render_array = [
              '#theme' => 'image_style',
              '#style_name' => 'banner_full',
              '#uri' => $url,
            ];
          }
        }
      }
    }

    return $render_array;

  }
}
