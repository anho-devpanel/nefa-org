<?php
/**
 * @file
 * Contains \Drupal\custom_ds_fields\Plugin\DsField\FunderLogoName.
 */

namespace Drupal\project_ds_code_fields\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\file\Entity\File;

/**
 * Plugin that conditionally render funder logo or name field.
 *
 * @DsField(
 *   id = "funder_logo_name",
 *   title = @Translation("DS: Funder Logo or Name"),
 *   entity_type = "paragraph",
 *   provider = "project_ds_code_fields",
 *   ui_limit = {"funder|*"}
 * )
 */
class FunderLogoName extends DsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $entity = $this->entity();
    // Logo is empty if none is set
    $logo = ($entity->hasField('field_logo')) && (!$entity->get('field_logo')->isEmpty()) ? $entity->field_logo->entity->field_image : '';
    // Name is empty if none is set
    $name = $entity->hasField('field_funder_name') ? $entity->field_funder_name->value : '';

    if ($name && !$logo) {
      return ['#markup' => "<p class='funder-name'>" . $name . "</p>"];
    }

    if ($logo) {
      $file = File::load($logo->target_id);

      return [
        '#theme' => 'image_style',
        '#style_name' => 'scaled_original',
        '#uri' => $file->getFileUri(),
        '#alt' => $logo->first()->alt,
        '#title' => $name ? $name : '',
      ];
    }

    return [];
  }
}
