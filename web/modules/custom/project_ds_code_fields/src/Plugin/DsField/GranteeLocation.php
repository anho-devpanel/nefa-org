<?php
/**
 * @file
 * Contains \Drupal\custom_ds_fields\Plugin\DsField\ArtistLocation.
 */

namespace Drupal\project_ds_code_fields\Plugin\DsField;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\file\Entity\File;

/**
 * Plugin that renders the grantee's location (city & state)
 *
 * @DsField(
 *   id = "grantee_location",
 *   title = @Translation("Location"),
 *   entity_type = "node",
 *   provider = "project_ds_code_fields",
 *   ui_limit = {"grant_recipient|full"}
 * )
 */
class GranteeLocation extends DsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Fetch the entity
    $entity = $this->entity();
    $city = "";
    $state = "";
    $grantee_location = "";
    $render_array = [];

    // If city field exists
    if ($entity->hasField('field_city')) {
      $city = $entity->field_city->value;
    }
    // If state field exists
    if ($entity->hasField('field_state')) {
      $state = $entity->field_state->value;
    }

    if ($city && $state) {
      $grantee_location = $city . ", " . $state;
    } elseif ($city) {
      $grantee_location = $city;
    } elseif ($state) {
      $grantee_location = $state;
    }

    if ($grantee_location) {
      $render_array = ['#markup' => $grantee_location];
    }

    return $render_array;

  }
}
