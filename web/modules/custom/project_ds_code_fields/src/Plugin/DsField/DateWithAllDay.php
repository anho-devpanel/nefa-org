<?php
/**
 * @file
 * Contains \Drupal\custom_ds_fields\Plugin\DsField\NewsAuthorTitleLink.
 */

namespace Drupal\project_ds_code_fields\Plugin\DsField;

use Drupal\daterange_compact\Entity\DateRangeFormat;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\ds\Plugin\DsField\Date;

/**
 * DS field that displays a date taking into account an all-day option.
 * Necessary since the datetime module can't handle all-day events. For
 * simplicity, it uses only formatters from the daterange_compact module.
 *
 * @DsField(
 *   id = "date_with_all_day",
 *   title = @Translation("DS: Date with all-day option"),
 *   entity_type = "node",
 *   provider = "project_ds_code_fields",
 *   ui_limit = {"event|*"}
 * )
 */
class DateWithAllDay extends Date {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $field = $this->getFieldConfiguration();
    $date = $this->entity()->field_event_date->first();
    $formatter = \Drupal::service('daterange_compact.date_range.formatter');
    $date_format = str_replace('ds_post_date_', '', $field['formatter']);
    $after_date = '';

    if (($this->entity()->field_after_date) && ($this->entity()->field_after_date->value)) {
      $after_date = " " . $this->entity()->field_after_date->value;
    }

    if ($this->entity()->field_event_date_all_day->value) {
      $timezone = DateTimeItemInterface::STORAGE_TIMEZONE;
      $output = $formatter->formatDateRange($date->start_date->getTimestamp(), $date->end_date->getTimestamp(), $date_format, $timezone);
    }
    else {
      $timezone = date_default_timezone_get();
      $output = $formatter->formatDateTimeRange($date->start_date->getTimestamp(), $date->end_date->getTimestamp(), $date_format, $timezone);
    }

    return [
      '#markup' => $output . $after_date,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function formatters() {
    // Date-range compact formatters
    $ranges = DateRangeFormat::loadMultiple();

    foreach ($ranges as $key => $range) {
      $formatters[$key] = t("Compact range: @range", ['@range' => $range->label()]);
    }

    return $formatters;
  }
}
