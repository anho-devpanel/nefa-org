<?php
/**
 * @file
 * Contains \Drupal\custom_ds_fields\Plugin\DsField\NDPNTPTitle.
 */

namespace Drupal\project_ds_code_fields\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;

/**
 * Plugin that renders the NDP/NTP role label.
 *
 * @DsField(
 *   id = "ndp_ntp_title",
 *   title = @Translation("DS: NDP/NTP Title"),
 *   entity_type = "node",
 *   provider = "project_ds_code_fields",
 *   ui_limit = {"news_author|*"}
 * )
 */
class NDPNTPTitle extends DsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Fetch the entity
    $entity = $this->entity();
    $role_name = "";
    $display_title = "";
    $render_array = [];

    // If role field exists
    if ($entity->hasField('field_roles')) {
      $field = $entity->field_roles;
      if ($field->value) {
        $role = $entity->field_roles[0]->value;
        // and its value is Advisory Council
        if (($role == 'ndpa') || ($role == 'ntpa')) {
          // get the label
          $role_name = $field->getFieldDefinition()
            ->getFieldStorageDefinition()
            ->getOptionsProvider('value', $field->getEntity())
            ->getPossibleOptions()[$field->value];

          $display_title = $role_name;

        }
      }
    }

    $render_array = ['#markup' => $display_title];

    return $render_array;

  }
}
