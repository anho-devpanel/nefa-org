<?php
/**
 * @file
 * Contains \Drupal\custom_ds_fields\Plugin\DsField\AcTitle.
 */

namespace Drupal\project_ds_code_fields\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;

/**
 * Plugin that renders the Advisory Council role label preceded by the title if one exists.
 *
 * @DsField(
 *   id = "ac_title",
 *   title = @Translation("DS: AC Title"),
 *   entity_type = "node",
 *   provider = "project_ds_code_fields",
 *   ui_limit = {"news_author|*"}
 * )
 */
class AcTitle extends DsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Fetch the entity
    $entity = $this->entity();
    $role_name = "";
    $title = "";
    $display_title = "";
    $render_array = [];

    // If role field exists
    if ($entity->hasField('field_roles')) {
      $field = $entity->field_roles;
      if ($field->value) {
        $role = $entity->field_roles[0]->value;
        // and its value is Advisory Council
        if ($role == 'ac') {
          // get the label
          $role_name = $field->getFieldDefinition()
            ->getFieldStorageDefinition()
            ->getOptionsProvider('value', $field->getEntity())
            ->getPossibleOptions()[$field->value];
          // if the Advisory Council title has been set as well
          if ($entity->hasField('field_advisory_council_title')) {
            $title = $entity->field_advisory_council_title->value;
            // display the title before the role naem
            if ($title) {
              $display_title = $title . ", " . $role_name;
              // or just the role name if no title exists
            } else {
              $display_title = $role_name;
            }
          }

        }
      }
    }

    $render_array = ['#markup' => $display_title];

    return $render_array;

  }
}
