<?php
/**
 * @file
 * Contains \Drupal\custom_ds_fields\Plugin\DsField\NewsTagsk.
 */

namespace Drupal\project_ds_code_fields\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Plugin that renders the the news tags
 *
 * @DsField(
 *   id = "news_tags",
 *   title = @Translation("DS: News Tags"),
 *   entity_type = "node",
 *   provider = "project_ds_code_fields",
 *   ui_limit = {"news|*"}
 * )
 */
class NewsTags extends DsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Fetch the entity
    $entity = $this->entity();
    $topic_name = "";
    $type_name = "";
    $topic_tags = [];
    $type_tags = [];
    $news_tags = [];
    $tab_label = "";
    $news_view_nid = '10385'; // nid of the node with the News view we need to filter
    $url = 'internal:/node/' . $news_view_nid;
    $render_array = [];

    // If topics field exists
    if ($entity->hasField('field_topic')) {
      // If it has a value
      if ($entity->field_topic->target_id) {
        foreach ($entity->field_topic as $index => $topic) {
          // We need to get the term name and not the id
          $term = Term::load($topic->target_id);
          if ($term) {
            $filter = "field_topic_target_id[" . $topic->target_id . "]"; // the filter identifier in the News view
            $topic_name = $term->getName();
            $options = [
              'query' => [$filter => $topic->target_id],
            ];

            $linked_topic_name = Link::fromTextAndUrl(t($topic_name), Url::fromUri($url, $options))->toString();
            $topic_tags[] = $linked_topic_name;
          }
        }
        //$tab_label = "<span class='tag-label'>Tags</span>";
        //$topic_list = implode(', ', $news_tags);
        //$render_array = ['#markup' => $tab_label . "<span class='news-tags'>" . $topic_list . "</span>"];
      }
    }

    // If news type field exists
    if ($entity->hasField('field_news_type')) {
      // If it has a value
      if ($entity->field_news_type->target_id) {
        foreach ($entity->field_news_type as $index => $type) {
          // We need to get the term name and not the id
          $term = Term::load($type->target_id);
          if ($term) {
            $filter = "field_news_type_target_id[" . $type->target_id . "]"; // the filter identifier in the News view
            $type_name = $term->getName();
            $options = [
              'query' => [$filter => $type->target_id],
            ];

            $linked_type_name = Link::fromTextAndUrl(t($type_name), Url::fromUri($url, $options))->toString();
            $type_tags[] = $linked_type_name;
          }
        }
        //$tab_label = "<span class='tag-label'>Tags</span>";
        //$type_list = implode(', ', $news_tags);
        //$render_array = ['#markup' => $tab_label . "<span class='news-tags'>" . $type_list . "</span>"];
      }
    }

    if ($topic_tags || $type_tags) {
      $tab_label = "<span class='tag-label'>Tags</span>";
      $news_tags = array_merge($topic_tags,$type_tags);
      $combined_tags = implode(', ', $news_tags);
      $render_array = ['#markup' => $tab_label . "<span class='news-tags'>" . $combined_tags . "</span>"];
    }


    return $render_array;

  }
}
