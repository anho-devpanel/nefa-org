<?php
/**
 * @file
 * Contains \Drupal\custom_ds_fields\Plugin\DsField\PublicationTitleLink.
 */

namespace Drupal\project_ds_code_fields\Plugin\DsField;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\file\Entity\File;

/**
 * Plugin that renders Publication titles linked to the publication file (pdf).
 *
 * @DsField(
 *   id = "publication_title_link",
 *   title = @Translation("DS: Publication Title Link"),
 *   entity_type = "node",
 *   provider = "project_ds_code_fields",
 *   ui_limit = {"publication|*"}
 * )
 */
class PublicationTitleLink extends DsFieldBase {


  /**
   * {@inheritdoc}
   */
  public function build() {
    // Fetch the entity
    $entity = $this->entity();
    $title = $entity->label(); // Title of the Publication node (this entity)
    $linked_title = "";
    $render_array = [];

    // If Course link field exists
    if ($entity->hasField('field_publication_file')) {
      $file_field = $entity->field_publication_file->entity->field_document; // the file field on the referenced media entity
      if ($file_field) {
        $file = File::load($file_field->target_id);
        $url = file_create_url($file->getFileUri());
        $path = file_url_transform_relative($url); // the url needs to be relative (i.e. sites/default/files, etc)
        // Could not get any of the D8 link methods to work so we are going old school
        $linked_title = "<h2><a href='" . $path . "'>" . $title . "</a></h2>";
      }
    }

    if ($linked_title) {
      $render_array = ['#markup' => $linked_title];
    } else {
      $render_array = ['#markup' => $title];
    }

    return $render_array;

  }
}
