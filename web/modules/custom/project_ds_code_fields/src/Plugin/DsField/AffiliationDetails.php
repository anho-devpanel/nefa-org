<?php
/**
 * @file
 * Contains \Drupal\custom_ds_fields\Plugin\DsField\AffiliationDetails.
 */

namespace Drupal\project_ds_code_fields\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\file\Entity\File;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Plugin that renders the affiliation details.
 *
 * @DsField(
 *   id = "affiliation_details",
 *   title = @Translation("DS: Affiliation Details"),
 *   entity_type = "paragraph",
 *   provider = "project_ds_code_fields",
 *   ui_limit = {"*|*"}
 * )
 */
class AffiliationDetails extends DsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Fetch the entity
    $entity = $this->entity();
    $affiliation = "";
    $affiliation_name = "";
    $affiliation_title = "";
    $website = "";
    $render_array = [];

    // If affiliation title field exists
    if ($entity->hasField('field_affiliation_title')) {
      $affiliation_title = $entity->field_affiliation_title->value;
    }

    // If affiliation name field exists
    if ($entity->hasField('field_affiliation_name')) {
      $affiliation_name = $entity->field_affiliation_name->value;
    }

    // If website field exists
    if ($entity->hasField('field_affiliation_website')) {
      $website = $entity->field_affiliation_website->uri;
    }

    // Run the conditions for the various scenarios

    // if name and website exists, link the name
    if ($affiliation_name && $website) {
      $affiliation = Link::fromTextAndUrl(t($affiliation_name), Url::fromUri($website))->toString();
      // or just name
    } else if ($affiliation_name) {
      $affiliation = $affiliation_name;
    }
    // if title and affiliation exist, show both separated by a comma
    if ($affiliation_title && $affiliation) {
      $render_array = ['#markup' => "<div class='affiliation-title'>" . $affiliation_title . "</div><div class='affiliation-name'>" . $affiliation . "</div>"];
      // or just the title
    } else if ($affiliation_title) {
      $render_array = ['#markup' => "<div class='affiliation-title'>" . $affiliation_title . "</div>"];
      // or just the name of the affiliation
    } else if ($affiliation) {
      $render_array = ['#markup' => "<div class='affiliation-name'>" . $affiliation . "</div>"];
    }

    return $render_array;
  }
}
