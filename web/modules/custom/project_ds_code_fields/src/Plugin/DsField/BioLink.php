<?php
/**
 * @file
 * Contains \Drupal\custom_ds_fields\Plugin\DsField\BioLink.
 */

namespace Drupal\project_ds_code_fields\Plugin\DsField;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\ds\Plugin\DsField\DsFieldBase;

/**
 * Plugin that renders a link to the node if the body (bio) has a value.
 *
 * @DsField(
 *   id = "bio_link",
 *   title = @Translation("DS: Bio Links"),
 *   entity_type = "node",
 *   provider = "project_ds_code_fields",
 *   ui_limit = {"*|*"}
 * )
 */
class BioLink extends DsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Fetch the entity
    $entity = $this->entity();
    $nid = $entity->id();
    $body = "";
    $bio_link = "";
    $render_array = [];

    // If body field exists
    if (isset($entity->body)) {
      $body = $entity->body->value;

      // If body value exists
      if (!empty($body)) {
        $bio_link = Link::fromTextAndUrl(t('Read Bio'), Url::fromRoute('entity.node.canonical', ['node' => $nid]))->toString();

        $render_array = array(
          '#markup' => $bio_link,
        );
      }
    }
    return $render_array;
  }

}
