<?php
/**
 * @file
 * Contains \Drupal\custom_ds_fields\Plugin\DsField\NewsAuthorTitleLink.
 */

namespace Drupal\project_ds_code_fields\Plugin\DsField;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\file\Entity\File;

/**
 * Plugin that renders the small banner image.
 *
 * @DsField(
 *   id = "small_image",
 *   title = @Translation("DS: Small banner image"),
 *   entity_type = "node",
 *   provider = "project_ds_code_fields",
 *   ui_limit = {"*|banner"}
 * )
 */
class SmallImage extends DsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Fetch the entity
    $entity = $this->entity();
    $banner_type_landing = "";
    $banner_type = "";
    $image = "";
    $render_array = [];

    // There are two different versions of a "banner type" field but both include an
    // option for "small-image"

    // If Banner Type (Landing) field exists
    if ($entity->hasField('field_banner_type_landing')) {
      $banner_type_landing = $entity->field_banner_type_landing->value;
    }
    // If Banner Type field exists
    if ($entity->hasField('field_banner_type')) {
      $banner_type = $entity->field_banner_type->value;
    }

      // Images are only relevant if it is a small image banner
    if (($banner_type_landing) && ($banner_type_landing == 'small-image') ||
      ($banner_type) && ($banner_type == 'small-image')) {

        // If an image exists
        if (($entity->hasField('field_main_image') && (!$entity->get('field_main_image')->isEmpty()))) {
          $image = $entity->field_main_image->entity->field_image;
          if ($image) {
            $file = File::load($image->target_id);
            $url = $file->getFileUri();
            $render_array = [
              '#theme' => 'image_style',
              '#style_name' => 'banner_small',
              '#uri' => $url,
            ];
          }
        }
      }

    return $render_array;

  }
}
