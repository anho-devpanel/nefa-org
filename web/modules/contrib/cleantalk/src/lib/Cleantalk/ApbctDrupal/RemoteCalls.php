<?php

namespace Cleantalk\ApbctDrupal;

class RemoteCalls extends \Cleantalk\Common\RemoteCalls {

  /**
   * SFW update
   *
   * @return string
   */
  public function action__sfw_update() {
    return \Drupal\cleantalk\CleantalkFuncs::apbct_sfw_update($this->api_key);
  }

  /**
   * SFW send logs
   *
   * @return string
   */
  public function action__sfw_send_logs() {
    return \Drupal\cleantalk\CleantalkFuncs::apbct_sfw_send_logs($this->api_key);
  }

  public function action__sfw_update__write_base() {
    return \Drupal\cleantalk\CleantalkFuncs::apbct_sfw_update($this->api_key);
  }

  /**
   * Get available remote calls from the storage.
   *
   * @return array
   */
  protected function getAvailableRcActions() {
    $remote_calls = \Drupal::state()->get('cleantalk_remote_calls');
    if ($remote_calls && !empty($remote_calls)) {
      return empty(array_diff_key($remote_calls, $this->default_rc)) ? $remote_calls : $this->default_rc;
    }
    return $this->default_rc;
  }

  /**
   * Set last call timestamp and save it to the storage.
   *
   * @param string $action
   *
   * @return void
   */
  protected function setLastCall($action) {
    // TODO: Implement setLastCall() method.
    $remote_calls = $this->getAvailableRcActions();
    $remote_calls[$action]['last_call'] = time();
    \Drupal::state()->set('cleantalk_remote_calls', $remote_calls);
  }

}
