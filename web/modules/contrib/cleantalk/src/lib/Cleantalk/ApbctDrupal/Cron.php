<?php

namespace Cleantalk\ApbctDrupal;

class Cron extends \Cleantalk\Common\Cron {

    public function saveTasks($tasks)
    {
        \Drupal::state()->set($this->cron_option_name, array('last_start' => time(), 'tasks' => $tasks));
    }

    /**
     * Getting all tasks
     *
     * @return array
     */
    public function getTasks()
    {
        $cron =  \Drupal::state()->get($this->cron_option_name);
        return (!empty($cron) && isset($cron['tasks'])) ? $cron['tasks'] : null;
    }

    /**
     * Save option with tasks
     *
     * @return int timestamp
     */
    public function getCronLastStart()
    {
        $cron =  \Drupal::state()->get($this->cron_option_name);
        return (!empty($cron) && isset($cron['last_start'])) ? $cron['last_start']: 0;
    }

    /**
     * Save timestamp of running Cron.
     *
     * @return bool
     */
    public function setCronLastStart()
    {
        \Drupal::state()->set($this->cron_option_name, array('last_start' => time(), 'tasks' => $this->getTasks()));
        return true;
    }
}