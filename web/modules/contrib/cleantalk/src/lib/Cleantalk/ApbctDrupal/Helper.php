<?php

namespace Cleantalk\ApbctDrupal;

class Helper extends \Cleantalk\Common\Helper {

    /**
     * Get fw stats from the storage.
     *
     * @return array
     * @example array( 'firewall_updating' => false, 'firewall_updating_id' => md5(), 'firewall_update_percent' => 0, 'firewall_updating_last_start' => 0 )
     * @important This method must be overloaded in the CMS-based Helper class.
     */
    public static function getFwStats()
    {
        return array('firewall_updating_id' => \Drupal::state()->get('firewall_updating_id') ? \Drupal::state()->get('firewall_updating_id') : null, 'firewall_updating_last_start' => \Drupal::state()->get('firewall_updating_last_start') ? \Drupal::state()->get('firewall_updating_last_start') : 0, 'firewall_update_percent' => \Drupal::state()->get('firewall_update_percent') ? \Drupal::state()->get('firewall_update_percent') : null);
    }

    /**
     * Save fw stats on the storage.
     *
     * @param array $fw_stats
     * @return bool
     * @important This method must be overloaded in the CMS-based Helper class.
     */
    public static function setFwStats( $fw_stats )
    {
        \Drupal::state()->set('firewall_updating_id',isset($fw_stats['firewall_updating_id']) ? $fw_stats['firewall_updating_id'] : null);
        \Drupal::state()->set('firewall_updating_last_start',isset($fw_stats['firewall_updating_last_start']) ? $fw_stats['firewall_updating_last_start'] : 0);
        \Drupal::state()->set('firewall_update_percent',isset($fw_stats['firewall_update_percent']) ? $fw_stats['firewall_update_percent'] : 0);
    }

    /**
     * Implement here any actions after SFW updating finished.
     *
     * @return void
     */
    public static function SfwUpdate_DoFinisnAction()
    {
        \Drupal::state()->set('cleantalk_sfw_last_check',time());
    }
}