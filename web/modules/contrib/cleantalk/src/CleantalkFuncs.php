<?php

namespace Drupal\cleantalk;

// Autoload
require_once(dirname(__FILE__) . '/lib/autoload.php');

//Antispam classes
use Cleantalk\Antispam\Cleantalk;
use Cleantalk\Antispam\CleantalkRequest;
use Cleantalk\Antispam\CleantalkResponse;

//Common classes
use Cleantalk\Common\API as CleantalkAPI;
use Cleantalk\ApbctDrupal\Helper as CleantalkHelper;
use Cleantalk\Common\Firewall\Firewall;
use Cleantalk\ApbctDrupal\RemoteCalls;
use Cleantalk\ApbctDrupal\Cron;
use Cleantalk\ApbctDrupal\DB;
use Cleantalk\Common\Variables\Server;
use Cleantalk\Common\Firewall\Modules\SFW;



use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

if ( ! defined( 'CLEANTALK_USER_AGENT' ) )
  define( 'CLEANTALK_USER_AGENT', 'drupal9-115' );

// Sessions
if ( ! defined( 'APBCT_SESSION__LIVE_TIME' ) )
  define('APBCT_SESSION__LIVE_TIME', 86400*2);
if ( ! defined( 'APBCT_SESSION__CHANCE_TO_CLEAN' ) )
  define('APBCT_SESSION__CHANCE_TO_CLEAN', 100);

if (!defined('APBCT_TBL_FIREWALL_DATA'))
  define('APBCT_TBL_FIREWALL_DATA', 'cleantalk_sfw');      // Table with firewall data.
if (!defined('APBCT_TBL_FIREWALL_LOG'))
  define('APBCT_TBL_FIREWALL_LOG',  'cleantalk_sfw_logs'); // Table with firewall logs.
if (!defined('APBCT_TBL_AC_LOGS'))
  define('APBCT_TBL_AC_LOGS',        'cleantalk_ac_logs');   // Table with firewall logs.
if (!defined('APBCT_TBL_AC_UA_BL'))
  define('APBCT_TBL_AC_UA_BL',      'cleantalk_ac_ua_bl');    // Table with User-Agents blacklist.
if (!defined('APBCT_TBL_SESSIONS'))
  define('APBCT_TBL_SESSIONS',      'cleantalk_sessions'); // Table with session data.
if (!defined('APBCT_SPAMSCAN_LOGS'))
  define('APBCT_SPAMSCAN_LOGS',     'cleantalk_spamscan_logs'); // Table with session data.
if (!defined('APBCT_SELECT_LIMIT'))
  define('APBCT_SELECT_LIMIT',      5000); // Select limit for logs.
if (!defined('APBCT_WRITE_LIMIT'))
  define('APBCT_WRITE_LIMIT',       5000); // Write limit for firewall data.

/**
 * Cleantalk class create request
 */

class CleantalkFuncs {

  /*
   * get form submit_time
  */

  static public function _cleantalk_get_submit_time($timestamp) {

    return self::_cleantalk_apbct_cookies_test() == 1 && $timestamp ? time() - (int)$timestamp : null;

  }

  /**
   * Save our variables into cookies OR sessions
   *
   * @param $name     string   Name of our variables to save
   * @param $value    string   Value of our variables to save
   */
  static public function apbct_setcookie($name, $value)
  {
    if (\Drupal::config('cleantalk.settings')->get('cleantalk_set_cookies')) {
      if (\Drupal::config('cleantalk.settings')->get('cleantalk_alternative_cookies_session')) {

        self::_apbct_alt_sessions__remove_old();

        // Into database
        $connection = \Drupal::database();
        $connection->query("INSERT INTO {cleantalk_sessions}
        (id, name, value, last_update)
        VALUES (:id, :name, :value, :last_update)
        ON DUPLICATE KEY UPDATE
        value = :value,
        last_update = :last_update", array(
          ':id' => self::_apbct_alt_session__id__get(),
          ':name' => $name,
          ':value' => $value,
          ':last_update' => date('Y-m-d H:i:s')
        ));

      } else {

        // Into cookies
        setcookie($name, $value, 0, '/');

      }
    }
  }

  /**
   * Get our variables from cookies OR sessions
   *
   * @param $name     string    Name of necessary variable to get
   *
   * @return string|null
   */
  static public function apbct_getcookie($name)
  {
    if (\Drupal::config('cleantalk.settings')->get('cleantalk_set_cookies')) {
        if (\Drupal::config('cleantalk.settings')->get('cleantalk_alternative_cookies_session') && (substr($name, 0, strlen('apbct')) === 'apbct')) {

        // From database
        $connection = \Drupal::database();
        $value = $connection->query("SELECT value FROM {cleantalk_sessions} WHERE id = :id AND name = :name",
          array(
            ':id' => self::_apbct_alt_session__id__get(),
            ':name' => $name
          ))->fetchField();
        if (false !== $value) {
          return $value;
        } else {
          return null;
        }

      } else {

        // From cookies
        if (isset($_COOKIE[$name])) {
          return $_COOKIE[$name];
        } else {
          return null;
        }

      }
    }
    return null;
  }

  /**
   * Clean 'cleantalk_sessions' table
   */
  static private function _apbct_alt_sessions__remove_old()
  {
    if (rand(0, 1000) < APBCT_SESSION__CHANCE_TO_CLEAN) {

      $connection = \Drupal::database();
      $connection->query("DELETE
      FROM {cleantalk_sessions}
      WHERE last_update < NOW() - INTERVAL ". APBCT_SESSION__LIVE_TIME ." SECOND
      LIMIT 100000;");

    }
  }

  /**
   * Get hash session ID
   *
   * @return string
   */
  static private function _apbct_alt_session__id__get()
  {
    $id = CleantalkHelper::ip__get(array('real'))
      . filter_input(INPUT_SERVER, 'HTTP_USER_AGENT')
      . filter_input(INPUT_SERVER, 'HTTP_ACCEPT_LANGUAGE');
    return hash('sha256', $id);
  }

  /**
   * Cookie test
   * @return int   1|0
   */
  static private function _cleantalk_apbct_cookies_test()
  {
    if (\Drupal::config('cleantalk.settings')->get('cleantalk_set_cookies')) {
      if (\Drupal::config('cleantalk.settings')->get('cleantalk_alternative_cookies_session')) {
        return 1;
      }

      $cookie_test = json_decode(stripslashes(self::apbct_getcookie('apbct_cookies_test')), true);

      if (is_null($cookie_test)) {
        return null;
      }

      $check_string = trim(\Drupal::config('cleantalk.settings')->get('cleantalk_authkey'));

      foreach ($cookie_test['cookies_names'] as $cookie_name) {
        $check_string .= self::apbct_getcookie($cookie_name);
      }
      unset($cokie_name);

      if ($cookie_test['check_value'] == md5($check_string)) {
        return 1;
      } else {
        return 0;
      }
    }
    return null;
  }

  /**
   * Cleantalk inner function - show error message and exit.
   */

  static public function _cleantalk_die($message) {

    $output = '<!DOCTYPE html><!-- Ticket #11289, IE bug fix: always pad the error page with enough characters such that it is greater than 512 bytes, even after gzip compression abcdefghijklmnopqrstuvwxyz1234567890aabbccddeeffgghhiijjkkllmmnnooppqqrrssttuuvvwwxxyyzz11223344556677889900abacbcbdcdcededfefegfgfhghgihihjijikjkjlklkmlmlnmnmononpopoqpqprqrqsrsrtstsubcbcdcdedefefgfabcadefbghicjkldmnoepqrfstugvwxhyz1i234j567k890laabmbccnddeoeffpgghqhiirjjksklltmmnunoovppqwqrrxsstytuuzvvw0wxx1yyz2z113223434455666777889890091abc2def3ghi4jkl5mno6pqr7stu8vwx9yz11aab2bcc3dd4ee5ff6gg7hh8ii9j0jk1kl2lmm3nnoo4p5pq6qrr7ss8tt9uuvv0wwx1x2yyzz13aba4cbcb5dcdc6dedfef8egf9gfh0ghg1ihi2hji3jik4jkj5lkl6kml7mln8mnm9ono--><html xmlns="http://www.w3.org/1999/xhtml" lang="en-US"><head>    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />    <title>Blacklisted</title>    <style type="text/css">        html {            background: #f1f1f1;        }        body {            background: #fff;            color: #444;            font-family: "Open Sans", sans-serif;            margin: 2em auto;            padding: 1em 2em;            max-width: 700px;            -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.13);            box-shadow: 0 1px 3px rgba(0,0,0,0.13);        }        h1 {            border-bottom: 1px solid #dadada;            clear: both;            color: #666;            font: 24px "Open Sans", sans-serif;            margin: 30px 0 0 0;            padding: 0;            padding-bottom: 7px;        }        #error-page {            margin-top: 50px;        }        #error-page p {            font-size: 14px;            line-height: 1.5;            margin: 25px 0 20px;        }        a {            color: #21759B;            text-decoration: none;        }        a:hover {            color: #D54E21;        }            </style></head><body id="error-page">    <p><center><b style="color: #49C73B;">Clean</b><b style="color: #349ebf;">Talk.</b> Spam protection</center><br><br>'.$message.'<script>setTimeout("history.back()", 5000);</script></p><p><a href="javascript:history.back()">&laquo; Back</a></p></body></html>';
    throw new ServiceUnavailableHttpException(3, $output);

  }

  /**
   * Cleantalk inner function - gets JavaScript checking value.
   */

  static public function _cleantalk_get_checkjs_value() {

    return md5(\Drupal::config('cleantalk.settings')->get("cleantalk_authkey") . '+' . \Drupal::config('system.site')->get("mail"));

  }

  /**
   * Cleantalk inner function - performs antispam checking.
   */

  static public function _cleantalk_check_spam($spam_check, $form_errors = null) {

    global $cleantalk_executed;

    $curr_user = \Drupal::currentUser();

    // Exclusion. Administrator.
    if ($curr_user->hasPermission('access administration menu') || $cleantalk_executed){
      return NULL;
    }

    // Exclusion. By roles.
    $roles = \Drupal::config('cleantalk.settings')->get('cleantalk_roles_exclusions');

    if (!empty($roles)){
      foreach ($roles as $role_id){
        if (self::_cleantalk_user_has_role_id($role_id, $curr_user)){
          return NULL;
        }
      }
    }

    // Exclusion. By number of posted comments
    if ($curr_user->id()){

      $user = \Drupal\user\Entity\User::load($curr_user->id());
      $uid = $user
        ->get('uid')
        ->value;

      // Don't check reged user with >= 'cleantalk_check_comments_min_approved' approved msgs.
      if (is_object($user) && $user->get('uid')->value > 0 && \Drupal::service('module_handler')->moduleExists('comment')){
        $result = \Drupal::database()
          ->query('SELECT count(*) AS count FROM {comment_field_data} WHERE uid=:uid AND status=1', [':uid' => $uid ])
          ->fetchObject()
          ->count;
        $count       = intval($result);
        $ct_comments = \Drupal::config('cleantalk.settings')
                              ->get('cleantalk_check_comments_min_approved');
        if ($count >= $ct_comments){
          return NULL;
        }
      }
    }

    // Exclusion. By URLs
    $url_exclusion = explode(",",\Drupal::config('cleantalk.settings')->get('cleantalk_url_exclusions'));
    if (is_array($url_exclusion) && count($url_exclusion)){

      $check_type = \Drupal::config('cleantalk.settings')
                           ->get('cleantalk_url_regexp');

      foreach ($url_exclusion as $key => $value){
        if (!empty($value)) {
          if ($check_type == 1){ // If RegExp
            if (@preg_match('/' . trim($value) . '/', $_SERVER['REQUEST_URI'])){
              return NULL;
            }
          }
          else{
            if (strpos($_SERVER['REQUEST_URI'], $value) !== false){ // Simple string checking
              return NULL;
            }
          }
          if (strpos(trim($value), 'node') !== FALSE && strpos($_SERVER['REQUEST_URI'], 'q=comment/reply/') !== FALSE){

            $get_node         = array_values(array_slice(explode('/', trim($value)), -1))[0];
            $current_reply_id = array_values(array_slice(explode('/', $_SERVER['REQUEST_URI']), -1))[0];

            if ($get_node == $current_reply_id){
              return NULL;
            }
          }
        }

      }
    }

    $ct_authkey = \Drupal::config('cleantalk.settings')->get('cleantalk_authkey');
    $ct_ws = self::_cleantalk_get_ws();

    if (!self::apbct_getcookie('ct_check_js')) {

      $checkjs = NULL;

    }

    elseif (self::apbct_getcookie('ct_check_js') == self::_cleantalk_get_checkjs_value()) {

      $checkjs = 1;

    }

    else {

      $checkjs = 0;

    }

    $ct = new Cleantalk();
    $ct->work_url = $ct_ws['work_url'];
    $ct->server_url = $ct_ws['server_url'];
    $ct->server_ttl = $ct_ws['server_ttl'];
    $ct->server_changed = $ct_ws['server_changed'];
      $role_exclusions = \Drupal::config('cleantalk.settings')->get('cleantalk_roles_exclusions');
    $ct_options=Array(
      'access_key'                            => $ct_authkey,
      'cleantalk_check_comments'              => \Drupal::config('cleantalk.settings')->get('cleantalk_check_comments'),
      'cleantalk_check_comments_automod'      => \Drupal::config('cleantalk.settings')->get('cleantalk_check_comments_automod'),
      'cleantalk_check_comments_min_approved' => \Drupal::config('cleantalk.settings')->get('cleantalk_check_comments_min_approved'),
      'cleantalk_check_register'              => \Drupal::config('cleantalk.settings')->get('cleantalk_check_register'),
      'cleantalk_check_webforms'              => \Drupal::config('cleantalk.settings')->get('cleantalk_check_webforms'),
      'cleantalk_check_contact_forms'         => \Drupal::config('cleantalk.settings')->get('cleantalk_check_contact_forms'),
      'cleantalk_check_forum_topics'          => \Drupal::config('cleantalk.settings')->get('cleantalk_check_forum_topics'),
      'cleantalk_check_search_form'           => \Drupal::config('cleantalk.settings')->get('cleantalk_check_search_form'),
      'cleantalk_url_exclusions'              => \Drupal::config('cleantalk.settings')->get('cleantalk_url_exclusions'),
      'cleantalk_url_regexp'                  => \Drupal::config('cleantalk.settings')->get('cleantalk_url_regexp'),
      'cleantalk_fields_exclusions'           => \Drupal::config('cleantalk.settings')->get('cleantalk_fields_exclusions'),
      'cleantalk_roles_exclusions'            => !empty($role_exclusions) ? implode(',', $role_exclusions) : '',
      'cleantalk_add_search_noindex'          => \Drupal::config('cleantalk.settings')->get('cleantalk_add_search_noindex'),
      'cleantalk_search_noindex'              => \Drupal::config('cleantalk.settings')->get('cleantalk_search_noindex'),
      'cleantalk_set_cookies'                 => \Drupal::config('cleantalk.settings')->get('cleantalk_set_cookies'),
      'cleantalk_alternative_cookies_session' => \Drupal::config('cleantalk.settings')->get('cleantalk_alternative_cookies_session'),
      'cleantalk_check_ccf'                   => \Drupal::config('cleantalk.settings')->get('cleantalk_check_ccf'),
      'cleantalk_check_external'              => \Drupal::config('cleantalk.settings')->get('cleantalk_check_external'),
      'cleantalk_link'                        => \Drupal::config('cleantalk.settings')->get('cleantalk_link'),
      'cleantalk_sfw'                         => \Drupal::config('cleantalk.settings')->get('cleantalk_sfw'),
    );

    $sender_info = \Drupal\Component\Serialization\Json::encode(
      // We have to use $_SERVER and $_COOKIE arrays instead \Drupal::request() for earlier access to this data from BootSubscriber
      array(
      'cms_lang'               => \Drupal::languageManager()->getCurrentLanguage()->getId(),
          'REFFERRER'              => isset($_SERVER['HTTP_REFERER'])                                ? htmlspecialchars($_SERVER['HTTP_REFERER'])                        : null,
          'page_url'               => isset($_SERVER['SERVER_NAME'], $_SERVER['REQUEST_URI'])        ? htmlspecialchars($_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']) : null,
          'USER_AGENT'             => isset($_SERVER['HTTP_USER_AGENT'])                             ? htmlspecialchars($_SERVER['HTTP_USER_AGENT'])                     : null,
      'ct_options'             => \Drupal\Component\Serialization\Json::encode($ct_options),
      'REFFERRER_PREVIOUS'     => self::apbct_getcookie('apbct_prev_referer'),
      'cookies_enabled'        => self::_cleantalk_apbct_cookies_test(),
      'fields_number'          => count($spam_check),
      'js_timezone'            => self::apbct_getcookie('ct_timezone'),
      'mouse_cursor_positions' => !empty($_COOKIE['ct_pointer_data'])                            ? json_decode(stripslashes($_COOKIE['ct_pointer_data']), true)      : null,
      'key_press_timestamp'    => !empty($_COOKIE['ct_fkp_timestamp'])                           ? $_COOKIE['ct_fkp_timestamp']                                      : null,
      'page_set_timestamp'     => !empty($_COOKIE['ct_ps_timestamp'])                            ? $_COOKIE['ct_ps_timestamp']                                       : null,
        'form_validation'        => ($form_errors && is_array($form_errors)) ? json_encode(array('validation_notice' => strip_tags(json_encode($form_errors)), 'page_url' => $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'])) : null,
      )
    );
    if ($spam_check['type'] == 'custom_contact_form' && isset($_SERVER['HTTP_REFERER']) && htmlspecialchars($_SERVER['HTTP_REFERER']) === 'https://www.google.com/') {
      $spam_check['type'] = 'site_search_drupal9';
    }
    $post_info = \Drupal\Component\Serialization\Json::encode(
      array(
      'comment_type' => $spam_check['type'],
      'post_url' => isset($_SERVER['HTTP_REFERER'])                                ? htmlspecialchars($_SERVER['HTTP_REFERER'])                        : null,
      )
    );
    $ct_request = new CleantalkRequest();
    $ct_request->auth_key = $ct_authkey;
    $ct_request->agent = CLEANTALK_USER_AGENT;
    $ct_request->response_lang = 'en';
    $ct_request->js_on = $checkjs;
    $ct_request->sender_info = $sender_info;
    $ct_request->post_info = $post_info;
    $ct_request->sender_email = $spam_check['sender_email'];
    $ct_request->sender_nickname = $spam_check['sender_nickname'];
    $ct_request->sender_ip = CleantalkHelper::ip__get(array('real'), false);
    $ct_request->x_forwarded_for = CleantalkHelper::ip__get(array('x_forwarded_for'), false);
    $ct_request->x_real_ip       = CleantalkHelper::ip__get(array('x_real_ip'), false);
    $ct_request->submit_time = isset($spam_check['multistep_submit_time']) ? self::_cleantalk_get_submit_time($spam_check['multistep_submit_time']) : self::_cleantalk_get_submit_time(self::apbct_getcookie('apbct_timestamp'));

    switch ($spam_check['type']) {

      case 'comment':

      case 'contact':

      case 'forum_topic':

      case 'webform':

			case 'site_search_drupal9':

      case 'custom_contact_form':

      case 'external_form':

        $timelabels_key = 'mail_error_comment';
        if (is_array($spam_check['message_body'])) {

          $spam_check['message_body'] = isset($spam_check['message_body']['message']) ? $spam_check['message_body']['message'] : implode("\n\n", $spam_check['message_body']);

        }

        $ct_request->message = $spam_check['message_title'] . " \n\n" . strip_tags($spam_check['message_body']);
        $ct_result = $ct->isAllowMessage($ct_request);

      break;

      case 'register':

        $timelabels_key = 'mail_error_reg';
        $ct_request->tz = $spam_check['timezone'];
        $ct_result = $ct->isAllowUser($ct_request);

      break;

    }

    $cleantalk_executed = true;;
    $ret_val = array();
    $ret_val['ct_request_id'] = $ct_result->id;

    if ($ct->server_change) {

      self::_cleantalk_set_ws($ct->work_url, $ct->server_ttl, time());

    }

    // First check errstr flag.

    if (!empty($ct_result->errstr) || (!empty($ct_result->inactive) && $ct_result->inactive == 1)) {

      // Cleantalk error so we go default way (no action at all).

      $ret_val['errno'] = 1;

      if($checkjs == 0) {

        $ret_val['allow'] = 0;

      }

      // Just inform admin.

      $err_title = $_SERVER['SERVER_NAME'] . ' - CleanTalk hook error';

      if (!empty($ct_result->errstr)) {

        $ret_val['errstr'] = self::_cleantalk_filter_response($ct_result->errstr);

      }

      else {

        $ret_val['errstr'] = self::_cleantalk_filter_response($ct_result->comment);

      }

      $send_flag = FALSE;

      $result = \Drupal::database()->select('cleantalk_timelabels', 'c')->fields('c', array('ct_value'))->condition('ct_key', $timelabels_key, '=')->execute();
      $results = $result->fetchCol(0);

      if (count($results) == 0) {

        $send_flag = TRUE;

      }

      elseif ($result->fetchObject() && \Drupal::time()->getRequestTime() - 900 > $result->fetchObject()->ct_value) {

        // 15 minutes.

        $send_flag = TRUE;

      }

      if ($send_flag) {

        \Drupal::database()->merge('cleantalk_timelabels')->key(array('ct_key' => $timelabels_key,))->fields(array('ct_value' => \Drupal::time()->getRequestTime(),))->execute();

        // @FIXME
        // // @FIXME
        // // This looks like another module's variable. You'll need to rewrite this call
        // // to ensure that it uses the correct configuration object.
        // $to = variable_get('site_mail', ini_get('sendmail_from'));

        if (!empty($to)) {

          drupal_mail("cleantalk", $timelabels_key, $to, language_default(), array('subject' => $err_title, 'body' => $ret_val['errstr'], 'headers' => array()), $to, TRUE);

        }

      }

      return $ret_val;

    }

    $ret_val['errno'] = 0;

    if ($ct_result->allow == 1) {

      // Not spammer.

      $ret_val['allow'] = 1;

      // Store request_id in globals to store it in DB later.

      self::_cleantalk_ct_result('set',$ret_val['allow'], $ct_result->id);

      // Don't store 'ct_result_comment', means good comment.
    }

    else {

      // Spammer.

      $ret_val['allow'] = 0;
      $ret_val['ct_result_comment'] = self::_cleantalk_filter_response($ct_result->comment);

      // Check stop_queue flag.

      if ($spam_check['type'] == 'comment') {

        // Spammer and stop_queue == 0 - to manual approvement.

        $ret_val['stop_queue'] = $ct_result->stop_queue;

        // Store request_id and comment in static to store them in DB later.

        self::_cleantalk_ct_result('set', $ct_result->id, $ret_val['allow'], $ret_val['ct_result_comment']);

      }

    }

    return $ret_val;

  }

  /**
   * Cleantalk inner function - performs CleanTalk comment|errstr filtering.
   */

  static public function _cleantalk_filter_response($ct_response) {

    if (preg_match('//u', $ct_response)) {

      $err_str = preg_replace('/\*\*\*/iu', '', $ct_response);

    }

    else {

      $err_str = preg_replace('/\*\*\*/i', '', $ct_response);

    }

    return \Drupal\Component\Utility\Xss::filter($err_str, array('a'));

  }

  /**
   * Cleantalk inner function - stores spam checking result.
   */

  static public function _cleantalk_ct_result($cmd = 'get', $id = '', $allow = 1, $comment = '') {

    static $request_id = '';
    static $result_allow = 1;
    static $result_comment = '';

    if ($cmd == 'set') {

      $request_id = $id;
      $result_allow = $allow;
      $result_comment = $comment;

    }

    else {

      return array(
        'ct_request_id' => $request_id,
        'ct_result_allow' => $result_allow,
        'ct_result_comment' => $result_comment,
      );

    }

  }

  /**
   * Cleantalk inner function - gets working server.
   */

  static public function _cleantalk_get_ws() {

    return array(
      'work_url' => \Drupal::state()->get('cleantalk_work_url'),
      'server_url' => 'http://moderate.cleantalk.org',
      'server_ttl' => \Drupal::state()->get('cleantalk_server_ttl'),
      'server_changed' => \Drupal::state()->get('cleantalk_server_changed'),
    );

  }

  /**
   * Cleantalk inner function - sets working server.
   */

  static public function _cleantalk_set_ws($work_url = 'http://moderate.cleantalk.org', $server_ttl = 0, $server_changed = 0) {

    \Drupal::state()->set('cleantalk_work_url',$work_url);
    \Drupal::state()->set('cleantalk_server_ttl',$server_ttl);
    \Drupal::state()->set('cleantalk_server_changed',$server_changed);

  }

  /**
   * Cleantalk inner function - check form handlers for save to prevent checking drafts/preview.
   */

  static public function _cleantalk_check_form_submit_handlers($submitHandlers) {

    if ($submitHandlers && is_array($submitHandlers)) {

      foreach ($submitHandlers as $handler) {

        if ($handler === '::save') {

          return true;

        }

      }

    }

    return false;

  }
  static public function apbct_sfw_update($access_key = '') {
      if( empty( $access_key ) ){
          $access_key = trim(\Drupal::config('cleantalk.settings')->get('cleantalk_authkey'));
          if (empty($access_key)) {
              return false;
          }
      }
      $firewall = new Firewall(
          $access_key,
          DB::getInstance(),
          APBCT_TBL_FIREWALL_LOG
      );
      $firewall->setSpecificHelper( new CleantalkHelper() );
      $fw_updater = $firewall->getUpdater( APBCT_TBL_FIREWALL_DATA );
      $fw_updater->update();

  }
  static public function apbct_sfw_send_logs($access_key = '') {
      if( empty( $access_key ) ){
          $access_key = trim(\Drupal::config('cleantalk.settings')->get('cleantalk_authkey'));
          if (empty($access_key)) {
              return false;
          }
      }

      $firewall = new Firewall( $access_key, DB::getInstance(), APBCT_TBL_FIREWALL_LOG );
      $firewall->setSpecificHelper( new CleantalkHelper() );
      $result = $firewall->sendLogs();

      return true;
  }

  static public function _cleantalk_user_has_role_id($role_id, $user = NULL){

    $roles =\Drupal::currentUser()->getRoles();

    if (is_array($roles) && in_array($role_id, $roles)){
      return TRUE;
    }

    return FALSE;
  }

  public static function cleantalk_get_user_roles() {

    $roles = \Drupal\user\Entity\Role::loadMultiple();
    $roles_arr = array();
    foreach($roles as $role) {
      $roles_arr[$role->get('id')] = $role->get('label');
    }
    asort($roles_arr);
    return $roles_arr;

  }

  public static function cleantalk_get_user_roles_default() {

    if( empty(\Drupal::config('cleantalk.settings')->get('cleantalk_roles_exclusions')) ) {
        $roles = self::cleantalk_get_user_roles();

        foreach( $roles as $role_id => $role_name ) {

          if(strpos('administrator', $role_id) === false) {
              unset( $roles[$role_id] );
        }
      }
      return array_keys($roles);
    } else {
      $roles = \Drupal::config('cleantalk.settings')->get('cleantalk_roles_exclusions');
      $config_roles = array();
      foreach( (array)$roles as $role => $role_name ) {
        $config_roles[$role_name] = $role_name;
      }
      return $config_roles;
    }

  }

  public static function print_form( $arr, $k ) {

    // Fix for pages04.net forms
    if( isset( $arr['formSourceName'] ) ){
      $tmp = array();
      foreach( $arr as $key => $val ){
        $tmp_key = str_replace( '_', '+', $key );
        $tmp[$tmp_key] = $val;
      }
      $arr = $tmp;
      unset( $tmp, $key, $tmp_key, $val );
    }

    foreach( $arr as $key => $value ){

      if( ! is_array( $value ) ){
        print '<textarea
				name="' . ( $k == '' ? $key : $k . '[' . $key . ']' ) . '"
				style="display:none;">' . htmlspecialchars( $value )
          . '</textarea>';
      }else{
        self::print_form( $value, $k == '' ? $key : $k . '[' . $key . ']' );
      }

    }

  }

  public static function ct_die( $comment ) {
    $err_text = '<center>' . ((defined('CLEANTALK_DISABLE_BLOCKING_TITLE') && CLEANTALK_DISABLE_BLOCKING_TITLE == true) ? '' : '<b style="color: #49C73B;">Clean</b><b style="color: #349ebf;">Talk.</b> ') . 'Spam protection' . "</center><br><br>\n<center>" . $comment . "</center>";
    $err_text .= '<script>setTimeout("history.back()", 5000);</script>';
    die( $err_text );
  }

  /**
   * AntiCrawler cookie value
   * @return false|string
   */
  public static function create_ac_cookie_value() {
    return hash( 'sha256', \Drupal::config('cleantalk.settings')->get("cleantalk_authkey") . md5( Server::get('HTTP_USER_AGENT') . md5( Server::get('HTTP_USER_AGENT') . Server::get('HTTPS') . Server::get('HTTP_HOST') )));
  }

}
