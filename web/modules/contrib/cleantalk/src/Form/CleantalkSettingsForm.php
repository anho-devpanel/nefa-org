<?php

namespace Drupal\cleantalk\Form;

// Autoload
require_once(dirname(__FILE__) . '/../lib/autoload.php');

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

use Cleantalk\Common\API as CleantalkAPI;
use Cleantalk\ApbctDrupal\Helper as CleantalkHelper;
use Drupal\cleantalk\CleantalkFuncs;
use Drupal\Core\Config\ConfigFactoryInterface;

class CleantalkSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */

  public function getFormId() {

    return 'cleantalk_settings_form';

  }

  /**
   * {@inheritdoc}
   */

  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('cleantalk.settings');

    $config_values = $form_state->getValues();

    foreach ($config_values as $key=>$value) {

      if (strpos($key, 'cleantalk') !== FALSE) {

        $config->set($key, $value);

      }

    }

    $config->save();

    if (method_exists($this, '_submitForm')) {

      $this->_submitForm($form, $form_state);

    }

    parent::submitForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */

  public function validateForm(array &$form, FormStateInterface $form_state) {

    $key_is_valid = CleantalkHelper::key_is_correct(trim($form_state->getValue('cleantalk_authkey')));
    $key_is_ok = false;

    if ($key_is_valid) {

      CleantalkAPI::method__send_empty_feedback($form_state->getValue('cleantalk_authkey'), CLEANTALK_USER_AGENT);
      $path_to_cms = preg_replace('/http[s]?:\/\//', '', $GLOBALS['base_url'], 1);
      $account_status = CleantalkAPI::method__notice_paid_till($form_state->getValue('cleantalk_authkey'), $path_to_cms);

      if (empty($account_status['error'])) {

        $key_is_ok = true;

        if ($form_state->getValue('cleantalk_sfw') === 1) {
          CleantalkFuncs::apbct_sfw_update($form_state->getValue('cleantalk_authkey'));
          CleantalkFuncs::apbct_sfw_send_logs($form_state->getValue('cleantalk_authkey'));
        }

      }

      \Drupal::state()->set('cleantalk_api_show_notice', (empty($account_status['error']) && isset($account_status['show_notice'])) ? $account_status['show_notice'] : 0);
      \Drupal::state()->set('cleantalk_api_renew', (empty($account_status['error']) && isset($account_status['renew'])) ? $account_status['renew'] : 0);
      \Drupal::state()->set('cleantalk_api_trial', (empty($account_status['error']) && isset($account_status['trial'])) ? $account_status['trial'] : 0);
      \Drupal::state()->set('cleantalk_api_user_token', (empty($account_status['error']) && isset($account_status['user_token'])) ? $account_status['user_token'] : '');
      \Drupal::state()->set('cleantalk_api_spam_count', (empty($account_status['error']) && isset($account_status['spam_count'])) ? $account_status['spam_count'] : 0);
      \Drupal::state()->set('cleantalk_api_moderate_ip', (empty($account_status['error']) && isset($account_status['moderate_ip'])) ? $account_status['moderate_ip'] : 0);
      \Drupal::state()->set('cleantalk_api_moderate', (empty($account_status['error']) && isset($account_status['moderate'])) ? $account_status['moderate'] : 0);
      \Drupal::state()->set('cleantalk_api_show_review', (empty($account_status['error']) && isset($account_status['show_review'])) ? $account_status['show_review'] : 0);
      \Drupal::state()->set('cleantalk_api_service_id', (empty($account_status['error']) && isset($account_status['service_id'])) ? $account_status['service_id'] : 0);
      \Drupal::state()->set('cleantalk_api_license_trial', (empty($account_status['error']) && isset($account_status['license_trial'])) ? $account_status['license_trial'] : 0);
      \Drupal::state()->set('cleantalk_api_account_name_ob', (empty($account_status['error']) && isset($account_status['account_name_ob'])) ? $account_status['account_name_ob'] : '');
      \Drupal::state()->set('cleantalk_api_ip_license', (empty($account_status['error']) && isset($account_status['ip_license'])) ? $account_status['ip_license'] : 0);
      \Drupal::state()->set('cleantalk_show_renew_banner', (\Drupal::state()->get('cleantalk_api_show_notice') && \Drupal::state()->get('cleantalk_api_trial')) ? 1 : 0);

    }

    // Turns off alternative cookies setting if cookies are disabled
    if( 0 == $form_state->getValue('cleantalk_set_cookies') )
    {
      $form_state->setValue('cleantalk_alternative_cookies_session', 0);
    }

    // Validating the URL exclusion string
    if( $form_state->getValue('cleantalk_url_regexp') ) {

      $errors = array();

      if( ! empty( $form_state->getValue('cleantalk_url_exclusions') ) ) {
        $exclusions = explode( ',', $form_state->getValue('cleantalk_url_exclusions') );
        foreach ( $exclusions as $exclusion ){
          $sanitized_exclusion = trim( $exclusion );
          if ( ! empty( $sanitized_exclusion ) ) {
            if( ! self::apbct_is_regexp( $sanitized_exclusion ) ) {
              $errors[] = $sanitized_exclusion;
            }
          }
        }
      }

      if( ! empty($errors) ) {
        // Remove the variable (setting) from BD if is not valid
        $config = \Drupal::service('config.factory')->getEditable('cleantalk.settings');
        $config->set( 'cleantalk_url_exclusions', '')->save();
        // And trigger an error
        $form_state->setErrorByName('cleantalk_url_exclusions', $this->t('URL exclusions is not valid: ') . implode( ', ', $errors ));
      }

    }

    if (!$key_is_ok) {

      $form_state->setErrorByName('cleantalk_authkey', $this->t('Access key is not valid.'));

    }

  }

  /**
   * {@inheritdoc}
   */

  protected function getEditableConfigNames() {

    return ['cleantalk.settings'];

  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {

    // EU Cookie Compilance cookie banner

    if (\Drupal::service('module_handler')->moduleExists('eu_cookie_compliance')) {
      $allowed_cookies = array('ct_check_js', 'ct_timezone', 'ct_pointer_data', 'ct_fkp_timestamp', 'ct_ps_timestamp', 'apbct_timestamp', 'apbct_cookies_test');
      $cooke_module_option = \Drupal::config('eu_cookie_compliance.settings')->get('allowed_cookies');
      $show_banner = false;
      foreach ($allowed_cookies as $cookie) {
        if (strpos($cooke_module_option, $cookie) === FALSE) {
          $show_banner = true;
        }
      }
      if ($show_banner) {
        \Drupal::messenger()->addMessage(t("EU Cookie Compliance module is enabled. Please, add an exclusion for <a href = 'https://cleantalk.org/help/install-drupal9#attention' target='_blank'>these</a> cookies."), 'warning', false);
      }
    }

    //Renew banner

    if (\Drupal::state()->get('cleantalk_show_renew_banner')) {

      $link = (\Drupal::state()->get('cleantalk_api_trial')) ? 'https://cleantalk.org/my/bill/recharge?utm_source=banner&utm_medium=wp-backend&utm_campaign=Drupal%20backend%20trial&user_token=' : 'https://cleantalk.org/my/bill/recharge?utm_source=banner&utm_medium=wp-backend&utm_campaign=Drupal%20backend%20renew&user_token=';

      \Drupal::messenger()->addMessage(t("Cleantalk module trial period ends, please upgrade to <a href='" . $link . \Drupal::state()->get('cleantalk_api_user_token') . "' target='_blank'><b>premium version</b></a> ."), 'warning', false);

    }

    if (\Drupal::state()->get('cleantalk_api_account_name_ob')) {

      $key_description = $this->t('Account at cleantalk.org is <b>' . \Drupal::state()->get('cleantalk_api_account_name_ob') . '</b>');

    }
    elseif (\Drupal::state()->get('cleantalk_api_moderate_ip') == 1) {

      $key_description = $this->t('The anti-spam service is paid by your hosting provider. License #<b>' . \Drupal::state()->get('cleantalk_api_ip_license') . '</b>');

    }
    else {

      $key_description = $this->t('Click <a target="_blank" href="http://cleantalk.org/register?platform=drupal">here</a> to get access key.');
    }

    $form['cleantalk_authkey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access key'),
      '#size' => 20,
      '#maxlength' => 20,
      '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_authkey') ? \Drupal::config('cleantalk.settings')->get('cleantalk_authkey') : '',
      '#description' => $key_description,
    ];

    $form['cleantalk_comments'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Comments'),
    );

    $form['cleantalk_comments']['cleantalk_check_comments'] = array(
    '#type' => 'checkbox',
    '#title' => $this->t('Check comments'),
    '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_check_comments'),
    '#description' => $this->t('Enabling this option will allow you to check all comments on your website.'),
    );

    $form['cleantalk_comments']['cleantalk_check_comments_automod'] = array(
    '#type' => 'checkbox',
    '#title' => $this->t('Enable automoderation'),
    '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_check_comments_automod'),
    '#description' => $this->t('Automatically put suspicious comments which may not be 100% spam to manual approvement and block obvious spam comments.').
    '<br /><span class="admin-missing">' .
    $this->t('Note: If disabled, all suspicious comments will be automatically blocked!') .
    '</span>',
    '#states' => array(
        // Only show this field when the value when checking comments is enabled
        'disabled' => array(
            ':input[name="cleantalk_check_comments"]' => array('checked' => FALSE),
        ),
      ),
    );

    $form['cleantalk_comments']['cleantalk_check_comments_min_approved'] = array(
      '#type' => 'number',
      '#title' => $this->t('Minimum approved comments per registered user'),
      '#min' => 1,
      '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_check_comments_min_approved'),
      '#description' => $this->t('Moderate messages of guests and registered users who have approved messages less than this value.'),
      '#states' => array(
          // Only show this field when the value when checking comments is enabled
          'disabled' => array(
              ':input[name="cleantalk_check_comments"]' => array('checked' => FALSE),
          ),
      ),
    );

    $form['cleantalk_search'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Search'),
    );

    $form['cleantalk_search']['cleantalk_check_search_form'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Check search form'),
      '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_check_search_form'),
      '#description' => $this->t('Enabling this option will allow you to check search form on your website.'),
    );

    $form['cleantalk_search']['cleantalk_search_noindex'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Add noindex for search form'),
      '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_search_noindex'),
      '#description' => $this->t('Add html meta-tag robots-noindex to search form.'),
    );

    $form['cleantalk_exclusions'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Exclusions'),
    );

    // Container URL_EXCLUSIONS
    $form['cleantalk_exclusions']['cleantalk_url_exclusions_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('URL exclusions'),
      '#description' => $this->t('Exclude urls from spam check. List them separated by commas.'),
    );
    $form['cleantalk_exclusions']['cleantalk_url_exclusions_fieldset']['cleantalk_url_exclusions_container_inline'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array(
          'container-inline'
        ),
      ),
    );
    $form['cleantalk_exclusions']['cleantalk_url_exclusions_fieldset']['cleantalk_url_exclusions_container_inline']['cleantalk_url_exclusions'] = array(
      '#type' => 'textfield',
      '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_url_exclusions'),
    );
    $form['cleantalk_exclusions']['cleantalk_url_exclusions_fieldset']['cleantalk_url_exclusions_container_inline']['cleantalk_url_regexp'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Regular expression'),
      '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_url_regexp'),
    );

    // Container FIELDS_EXCLUSIONS
    $form['cleantalk_exclusions']['cleantalk_fields_exclusions_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Fields exclusions'),
      '#description' => $this->t('Exclude fields from spam check. List them separated by commas. Works on forms except for registration and comment forms.'),
    );
    $form['cleantalk_exclusions']['cleantalk_fields_exclusions_fieldset']['cleantalk_fields_exclusions_container_inline'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array(
          'container-inline'
        ),
      ),
    );
    $form['cleantalk_exclusions']['cleantalk_fields_exclusions_fieldset']['cleantalk_fields_exclusions_container_inline']['cleantalk_fields_exclusions'] = array(
      '#type' => 'textfield',
      '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_fields_exclusions'),
    );

    // Container ROLES_EXCLUSIONS
    $form['cleantalk_exclusions']['cleantalk_roles_exclusions_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Roles checking'),
      '#description' => $this->t('Roles which bypass spam test. You can select multiple roles.'),
    );
    $form['cleantalk_exclusions']['cleantalk_roles_exclusions_fieldset']['cleantalk_roles_exclusions_container_inline'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array(
          'container-inline'
        ),
      ),
    );
    $form['cleantalk_exclusions']['cleantalk_roles_exclusions_fieldset']['cleantalk_roles_exclusions_container_inline']['cleantalk_roles_exclusions'] = array(
      '#type' => 'select',
      '#options' => CleantalkFuncs::cleantalk_get_user_roles(),
      '#multiple' => true,
      '#default_value' => CleantalkFuncs::cleantalk_get_user_roles_default(),
    );

    $form['cleantalk_check_register'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Check registrations'),
      '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_check_register'),
      '#description' => $this->t('Enabling this option will allow you to check all registrations on your website.'),
    );

    $form['cleantalk_check_webforms'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Check webforms'),
      '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_check_webforms'),
      '#description' => $this->t('Enabling this option will allow you to check all webforms on your website.'),
    );

    $form['cleantalk_check_contact_forms'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Check contact forms'),
      '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_check_contact_forms'),
      '#description' => $this->t('Enabling this option will allow you to check all contact forms on your website.'),
    );

    $form['cleantalk_check_forum_topics'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Check forum topics'),
      '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_check_forum_topics'),
      '#description' => $this->t('Enabling this option will allow you to check all forum topics on your website.'),
    );

    $form['cleantalk_check_ccf'] = array(
    '#type' => 'checkbox',
    '#title' => $this->t('Check custom forms'),
    '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_check_ccf'),
    '#description' => $this->t('Enabling this option will allow you to check all forms on your website.') .
    '<br /><span class="admin-missing">' .
    $this->t('Note: May cause conflicts!') .
    '</span>',
    );

    $form['cleantalk_check_external'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Check external forms'),
      '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_check_external'),
      '#description' => $this->t('Turn this option on to protect forms on your website that send data to third-part servers (like MailChimp).'),
    );

    $form['cleantalk_set_cookies'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Set cookies'),
      '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_set_cookies'),
      '#description' => $this->t('Turn this option off to deny plugin generates any cookies on website front-end. This option is helpful if you use Varnish. But most of contact forms will not be protected if the option is turned off!') . '<br /><span class="admin-disabled">' .
        $this->t('Note: We strongly recommend you to enable this otherwise it could cause false positives spam detection.') .
        '</span>',
    );

    $form['cleantalk_alternative_cookies_session'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Use alternative mechanism for cookies'),
      '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_alternative_cookies_session'),
      '#description' => $this->t('Doesn\'t use cookie or PHP sessions. Collect data for all types of bots.'),
      '#states' => array(
        // Only show this field when the value when checking comments is enabled
        'invisible' => array(
          ':input[name="cleantalk_set_cookies"]' => array('checked' => FALSE),
        ),
      ),
    );

    $form['cleantalk_sfw'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('SpamFireWall'),
      '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_sfw'),
      '#description' => $this->t('This option allows to filter spam bots before they access website. Also reduces CPU usage on hosting server and accelerates pages load time.'),
    ];

    $form['cleantalk_sfw_ac'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Anti-Crawler'),
      '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_sfw_ac'),
      '#description' => $this->t('Plugin shows SpamFireWall stop page for any bot, except allowed bots (Google, Yahoo and etc).
                                        Anti-Crawler includes blocking bots by the User-Agent. Use Personal lists in the Dashboard to filter specific User-Agents.'),
      '#states' => array(
        // Only show this field when SFW is enabled
        'invisible' => array(
          ':input[name="cleantalk_sfw"]' => array('checked' => FALSE),
        ),
      ),
    ];

    /**
     * Antiflood settings
     * ==================
     */
    $form['cleantalk_sfw_antiflood'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Anti-Flood'),
      '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_sfw_antiflood'),
      '#description' => $this->t('Shows the SpamFireWall page for bots trying to crawl your site. Look at the page limit setting below.'),
      '#states' => array(
        // Only show this field when SFW is enabled
        'invisible' => array(
          ':input[name="cleantalk_sfw"]' => array('checked' => FALSE),
        ),
      ),
    ];

    $form['cleantalk_sfw_antiflood_limit'] = array(
      '#type' => 'number',
      '#title' => $this->t('Anti-Flood Page Views Limit'),
      '#min' => 20,
      '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_sfw_antiflood_limit'),
      '#description' => $this->t('Count of page view per 1 minute before plugin shows SpamFireWall page. SpamFireWall page active for 30 second after that valid visitor (with JavaScript) passes the page to the demanded page of the site.'),
      '#states' => array(
        // Only show this field when the value Anti-flood is enabled
        'invisible' => array(
          ':input[name="cleantalk_sfw_antiflood"]' => array('checked' => FALSE),
        ),
      ),
    );

    $form['cleantalk_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Tell others about CleanTalk'),
      '#default_value' => \Drupal::config('cleantalk.settings')->get('cleantalk_link'),
      '#description' => $this->t('Checking this box places a small link under the comment form that lets others know what anti-spam tool protects your site.'),
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * Is this valid regexp
   *
   * @param $regexp
   * @return bool
   */
  private static function apbct_is_regexp($regexp ) {

    return @preg_match( '/' . $regexp . '/', null ) !== false;

  }

}
