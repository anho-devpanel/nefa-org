<?php

namespace Drupal\cleantalk\EventSubscriber;

// Autoload
require_once(dirname(__FILE__) . '/../lib/autoload.php');

use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;
//Antispam classes
use Cleantalk\Antispam\Cleantalk;
use Cleantalk\Antispam\CleantalkRequest;
use Cleantalk\Antispam\CleantalkResponse;

//Common classes
use Cleantalk\Common\API as CleantalkAPI;
use Cleantalk\ApbctDrupal\Helper as CleantalkHelper;
use Cleantalk\Common\Firewall\Firewall;
use Cleantalk\ApbctDrupal\RemoteCalls;
use Cleantalk\ApbctDrupal\Cron;
use Cleantalk\ApbctDrupal\DB;
use Cleantalk\Common\Variables\Server;
use Cleantalk\Common\Firewall\Modules\SFW;
use Cleantalk\Common\Firewall\Modules\AntiCrawler;
use Cleantalk\Common\Firewall\Modules\AntiFlood;

use Drupal\cleantalk\CleantalkFuncs;
use \Drupal\Component\Utility\Html;
if (!defined('APBCT_TBL_FIREWALL_DATA'))
  define('APBCT_TBL_FIREWALL_DATA', 'cleantalk_sfw');      // Table with firewall data.
if (!defined('APBCT_TBL_FIREWALL_LOG'))
  define('APBCT_TBL_FIREWALL_LOG',  'cleantalk_sfw_logs'); // Table with firewall logs.
if (!defined('APBCT_TBL_AC_LOGS'))
  define('APBCT_TBL_AC_LOGS',        'cleantalk_ac_logs');   // Table with firewall logs.
if (!defined('APBCT_TBL_AC_UA_BL'))
  define('APBCT_TBL_AC_UA_BL',      'cleantalk_ac_ua_bl');    // Table with User-Agents blacklist.
if (!defined('APBCT_TBL_SESSIONS'))
  define('APBCT_TBL_SESSIONS',      'cleantalk_sessions'); // Table with session data.
if (!defined('APBCT_SPAMSCAN_LOGS'))
  define('APBCT_SPAMSCAN_LOGS',     'cleantalk_spamscan_logs'); // Table with session data.
if (!defined('APBCT_SELECT_LIMIT'))
  define('APBCT_SELECT_LIMIT',      5000); // Select limit for logs.
if (!defined('APBCT_WRITE_LIMIT'))
  define('APBCT_WRITE_LIMIT',       5000); // Write limit for firewall data.

class BootSubscriber implements HttpKernelInterface  {

  /**
   * The wrapped HTTP kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */

  protected $httpKernel;

  /**
   * Creates a HTTP middleware handler.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $kernel
   *   The HTTP kernel.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */

  protected $messenger;

  public function __construct(HttpKernelInterface $kernel, MessengerInterface $messenger = null) {

    $this->httpKernel = $kernel;
    $this->messenger = $messenger;

  }

  /**
   * {@inheritdoc}
   */

  public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = TRUE) {

    if (strpos($request->server->get('REQUEST_URI'), '/admin/') === false) {

      // If Cookies are enabled and headers not sent
      if ( \Drupal::config('cleantalk.settings')->get('cleantalk_set_cookies') && ! headers_sent() ) {

        // Cookie names to validate
        $cookie_test_value = array(
          'cookies_names' => array(),
          'check_value' => trim(\Drupal::config('cleantalk.settings')->get('cleantalk_authkey')),
        );

        // Submit time
        $apbct_timestamp = time();
        // Fix for submit_time = 0
        if(\Drupal::config('cleantalk.settings')->get('cleantalk_alternative_cookies_session')){
          // by database
          $prev_time = CleantalkFuncs::apbct_getcookie('apbct_prev_timestamp');
          if(is_null($prev_time)){
            CleantalkFuncs::apbct_setcookie('apbct_timestamp', $apbct_timestamp);
            CleantalkFuncs::apbct_setcookie('apbct_prev_timestamp', $apbct_timestamp);
            $cookie_test_value['check_value'] .= $apbct_timestamp;
          } else {
            CleantalkFuncs::apbct_setcookie('apbct_timestamp', $prev_time);
            CleantalkFuncs::apbct_setcookie('apbct_prev_timestamp', $apbct_timestamp);
            $cookie_test_value['check_value'] .= $prev_time;
          }
        } else {
          // by cookies
          CleantalkFuncs::apbct_setcookie('apbct_timestamp', $apbct_timestamp);
          $cookie_test_value['check_value'] .= $apbct_timestamp;
        }
        $cookie_test_value['cookies_names'][] = 'apbct_timestamp';

        //Previous referer
        if (!empty($_SERVER['HTTP_REFERER'])) {
          CleantalkFuncs::apbct_setcookie('apbct_prev_referer', $_SERVER['HTTP_REFERER']);
          $cookie_test_value['cookies_names'][] = 'apbct_prev_referer';
          $cookie_test_value['check_value'] .= $_SERVER['HTTP_REFERER'];
        }

        // Cookies test
        $cookie_test_value['check_value'] = md5($cookie_test_value['check_value']);
        CleantalkFuncs::apbct_setcookie('apbct_cookies_test', json_encode($cookie_test_value));

      } // ENDIF: If Cookies are enabled and headers not sent

      // Remote calls
      if( RemoteCalls::check() ) {
          $rc = new RemoteCalls( trim(\Drupal::config('cleantalk.settings')->get('cleantalk_authkey')));
          $rc->perform();
      }
      $cron = new Cron();
      $cron_option = \Drupal::state()->get($cron->getCronOptionName());
      if (empty($cron_option)) {
          $cron->addTask( 'sfw_update', '\Drupal\cleantalk\CleantalkFuncs::apbct_sfw_update', 86400, time() + 60 );
          $cron->addTask( 'sfw_send_logs', '\Drupal\cleantalk\CleantalkFuncs::apbct_sfw_send_logs', 3600 );
      }
      $tasks_to_run = $cron->checkTasks(); // Check for current tasks. Drop tasks inner counters.

      if(
          ! empty( $tasks_to_run ) && // There is tasks to run
          ! RemoteCalls::check() && // Do not doing CRON in remote call action
          (
              ! defined( 'DOING_CRON' ) ||
              ( defined( 'DOING_CRON' ) && DOING_CRON !== true )
          )
      ){
          $cron_res = $cron->runTasks( $tasks_to_run );
          // Handle the $cron_res for errors here.
      }
      if (\Drupal::config('cleantalk.settings')->get('cleantalk_sfw')) {

        $ct_key = trim(\Drupal::config('cleantalk.settings')->get('cleantalk_authkey'));

        if($ct_key != '') {
          $firewall = new Firewall(
              $ct_key,
              DB::getInstance(),
              APBCT_TBL_FIREWALL_LOG
          );

          $firewall->loadFwModule( new SFW(
              APBCT_TBL_FIREWALL_DATA,
              array(
                  'sfw_counter'   => 0,
                  'cookie_domain' => Server::get('HTTP_HOST'),
                  'set_cookies'    => 1,
              )
          ) );

          /**
           * Anti-crawler module start
           */
          if (\Drupal::config('cleantalk.settings')->get('cleantalk_sfw_ac')) {
            $firewall->loadFwModule( new AntiCrawler(
              APBCT_TBL_AC_UA_BL,
              array(
                'db__table__ac_logs' => APBCT_TBL_AC_LOGS,
                'db__table__sfw_logs' => APBCT_TBL_FIREWALL_LOG,
                'db__table__sfw' => APBCT_TBL_FIREWALL_DATA
              )
            ) );
          }

          /**
           * Anti-flood module start
           */
          if(\Drupal::config('cleantalk.settings')->get('cleantalk_sfw_antiflood')) {
            $firewall->loadFwModule( new AntiFlood(
              APBCT_TBL_FIREWALL_LOG,
              array(
                'view_limit' => \Drupal::config('cleantalk.settings')->get('cleantalk_sfw_antiflood_limit'),
                'db__table__ac_logs' => APBCT_TBL_AC_LOGS
              )
            ));
          }

          $firewall->run();
        }

      }

      //Custom Contact forms
      if (count($_POST) > 0 && !$request->get('form_build_id') && !$request->get('form_id') && \Drupal::config('cleantalk.settings')->get('cleantalk_check_ccf')) {

        $ct_temp_msg_data = CleantalkHelper::get_fields_any($request);
        $spam_check = array();
        $spam_check['type'] = 'custom_contact_form';
        $spam_check['sender_email'] = ($ct_temp_msg_data['email']    ? $ct_temp_msg_data['email']    : '');
        $spam_check['sender_nickname'] = ($ct_temp_msg_data['nickname'] ? $ct_temp_msg_data['nickname'] : '');
        $spam_check['message_title'] = ($ct_temp_msg_data['subject']  ? $ct_temp_msg_data['subject']  : '');
        $spam_check['message_body'] = ($ct_temp_msg_data['message']  ? implode("\n", $ct_temp_msg_data['message'])  : '');

        if ($spam_check['sender_email'] != '' || $spam_check['message_title'] != '' || $spam_check['message_body'] != '') {

          $result = CleantalkFuncs::_cleantalk_check_spam($spam_check);

          if (isset($result) && is_array($result) && $result['errno'] == 0 && $result['allow'] != 1) {

            $this->messenger->addError(HTML::escape($result['ct_result_comment']));

          }

        }

      }

      // Search
      if( $request->server->get('REQUEST_URI') && strpos($request->server->get('REQUEST_URI'), 'search') !== false && $request->get('keys') ) {

        if (\Drupal::config('cleantalk.settings')->get('cleantalk_check_search_form')) {

          global $user;
          $get_query = $request->get('keys');

          $spam_check['type'] = 'site_search_drupal9';

          $spam_check['sender_email'] = !empty($user->mail) ? $user->mail : '';
          $spam_check['sender_nickname'] = !empty($user->name) ? $user->name : '';
          $spam_check['message_title'] = '';
          $spam_check['message_body'] = $get_query;

          if ($spam_check['sender_email'] != '' || $spam_check['message_body']) {

            $spam_result = CleantalkFuncs::_cleantalk_check_spam($spam_check);

            if (isset($spam_result) && is_array($spam_result) && $spam_result['errno'] == 0 && $spam_result['allow'] != 1)
              CleantalkFuncs::_cleantalk_die($spam_result['ct_result_comment']);
          }

        }

      }

      // External Forms
      if (
        count( $_POST ) > 0 &&
        \Drupal::config('cleantalk.settings')->get('cleantalk_check_external') &&
        $request->get( 'cleantalk_hidden_method' ) &&
        $request->get( 'cleantalk_hidden_action' )
      ) {

        $action = htmlspecialchars( $request->get( 'cleantalk_hidden_action' ) );
        $method = htmlspecialchars( $request->get( 'cleantalk_hidden_method' ) );
        $request->request->remove( 'cleantalk_hidden_action' );
        $request->request->remove( 'cleantalk_hidden_method' );

        $ct_temp_msg_data = CleantalkHelper::get_fields_any($request->request->all());
        $spam_check = array();
        $spam_check['type'] = 'external_form';
        $spam_check['sender_email'] = ($ct_temp_msg_data['email']    ? $ct_temp_msg_data['email']    : '');
        $spam_check['sender_nickname'] = ($ct_temp_msg_data['nickname'] ? $ct_temp_msg_data['nickname'] : '');
        $spam_check['message_title'] = ($ct_temp_msg_data['subject']  ? $ct_temp_msg_data['subject']  : '');
        $spam_check['message_body'] = ($ct_temp_msg_data['message']  ? implode("\n", $ct_temp_msg_data['message'])  : '');

        if ($spam_check['sender_email'] != '' || $spam_check['message_title'] != '' || $spam_check['message_body'] != '') {

          $result = CleantalkFuncs::_cleantalk_check_spam( $spam_check );
          if ( isset($result) && is_array($result) && $result['errno'] == 0 && $result['allow'] != 1 ) {

            // Do block
            CleantalkFuncs::ct_die(HTML::escape($result['ct_result_comment']));

          } else {

            // Do the form sending
            if( ! $request->isXmlHttpRequest() ){
              print "<html><body><form method='$method' action='$action'>";
              CleantalkFuncs::print_form($_POST, '');
              print "</form></body></html>";
              print "<script " . ( class_exists('Cookiebot_WP') ? 'data-cookieconsent="ignore"' : '' ) . ">
                if(document.forms[0].submit !== 'undefined'){
                  var objects = document.getElementsByName('submit');
                  if(objects.length > 0)
                    document.forms[0].removeChild(objects[0]);
                }
                document.forms[0].submit();
              </script>";
              die();
            }
          }

        }

      }

    }


    return $this->httpKernel->handle($request, $type, $catch);

  }

}
