var d = new Date(),
	ctTimeMs = new Date().getTime(),
	ctMouseEventTimerFlag = true, //Reading interval flag
	ctMouseData = "[",
	ctMouseDataCounter = 0;

function ctSetCookie(cookies) {
  let useAltCookies = ct_use_alt_cookies;

  if(useAltCookies === 1) {
    let xhr = new XMLHttpRequest();

    let json = JSON.stringify(cookies);

    xhr.open("POST", '/admin/config/cleantalk/set_alt_cookies')
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');

    xhr.send(json);
  } else {
    cookies.forEach(function (e) {
      document.cookie = e.name + "=" + encodeURIComponent(e.value) + "; path=/";
    })
  }
}

ctSetCookie(
  [
    {
      'name' : 'ct_check_js',
      'value' : ct_check_js_val
    },
    {
      'name' : 'ct_ps_timestamp',
      'value' : Math.floor(new Date().getTime()/1000)
    },
    {
      'name' : 'ct_fkp_timestamp',
      'value' : '0'
    },
    {
      'name' : 'ct_pointer_data',
      'value' : '0'
    },
    {
      'name' : 'ct_timezone',
      'value' : d.getTimezoneOffset()/60*(-1)
    },
    {
      'name' : 'apbct_antibot',
      'value' : drupal_ac_antibot_cookie_value
    }
  ]
);

//Reading interval

var ctMouseReadInterval = setInterval(function() {
		ctMouseEventTimerFlag = true;
	}, 150);

//Writting interval

var ctMouseWriteDataInterval = setInterval(function() {
		var ctMouseDataToSend = ctMouseData.slice(0,-1).concat("]");
		ctSetCookie([
      {
        'name' : 'ct_pointer_data',
        'value' : ctMouseDataToSend
      }
    ]);
	}, 1200);

//Stop observing function

function ctMouseStopData() {
	if(typeof window.addEventListener == "function")
		window.removeEventListener("mousemove", ctFunctionMouseMove);
	else
		window.detachEvent("onmousemove", ctFunctionMouseMove);
	clearInterval(ctMouseReadInterval);
	clearInterval(ctMouseWriteDataInterval);
}

//Logging mouse position each 300 ms

var ctFunctionMouseMove = function output(event) {
	if (ctMouseEventTimerFlag == true) {
		var mouseDate = new Date();
		ctMouseData += "[" + Math.round(event.pageY) + "," + Math.round(event.pageX) + "," + Math.round(mouseDate.getTime() - ctTimeMs) + "],";
		ctMouseDataCounter++;
		ctMouseEventTimerFlag = false;
		if(ctMouseDataCounter >= 100)
			ctMouseStopData();
	}
};

//Stop key listening function

function ctKeyStopStopListening() {
	if (typeof window.addEventListener == "function") {
		window.removeEventListener("mousedown", ctFunctionFirstKey);
		window.removeEventListener("keydown", ctFunctionFirstKey);
	}
	else {
		window.detachEvent("mousedown", ctFunctionFirstKey);
		window.detachEvent("keydown", ctFunctionFirstKey);
	}
}

//Writing first key press timestamp

var ctFunctionFirstKey = function output(event) {
	var KeyTimestamp = Math.floor(new Date().getTime()/1000);
  ctSetCookie([
    {
      'name' : 'ct_fkp_timestamp',
      'value' : KeyTimestamp
    }
  ]);
	ctKeyStopStopListening();
};

if (typeof window.addEventListener == "function") {
	window.addEventListener("mousemove", ctFunctionMouseMove);
	window.addEventListener("mousedown", ctFunctionFirstKey);
	window.addEventListener("keydown", ctFunctionFirstKey);
}
else {
	window.attachEvent("onmousemove", ctFunctionMouseMove);
	window.attachEvent("mousedown", ctFunctionFirstKey);
	window.attachEvent("keydown", ctFunctionFirstKey);
}
